# Pascal Hexagon

by Eduardo Mezêncio

## Introduction

Pascal Hexagon is a free software 'clone' of Terry Cavanagh's Super Hexagon,
made in the Pascal programming language using the ZenGL library.

The stages in Pascal Hexagon are human-readable text files (well, readable as
long as you're ok with reading lots of numbers) that anyone can create. For now,
it comes bundled with only one demo stage, but if anyone come up with some nice
stages, I will add them to the package. The stage format is detailed below.

## Screenshots

![Title Screenshot](http://i.imgur.com/icu8jE2.png)

![Game Screenshot](http://i.imgur.com/LpMdBx2.png)

## Compiling

Pascal Hexagon can be compiled for gnu/linux and windows, for now. Just run
`make` to compile, as long as you have *fpc* and gnu *make* available.

## Running

To run the game, you need to have the game data from
https://gitlab.com/pascalhexagon/pascalhexagon-data in a folder named **data**.
The **data** folder must be located in the same folder from which you run the
executable.

## Installing

For now, there's no install script. You will have to install the game manually
if you want to, or play it without installing. A desktop file is provided in the
res folder for gnu/linux systems, that you can copy to your applications folder
and then edit to point to the correct paths of the executable and icon. There's
a nice icon bundled too!

## License

See the LICENSE file.

ZenGL is copyrighted by Andrey Kemka and his license states that "altered source
versions must be plainly marked as such". Pascal Hexagon includes a stripped
down version of ZenGL, with only the needed parts for the project. For the full
ZenGL code, visit zengl.org.

## The stage format

Take a look at the demo stage in data/stages, you can just modify it to create
your own. Here's an explanation of the stage format:

**Note**: I wrote this more than one year after programming the game, so there
may be some mistakes about the stage format. If something's strange, report to
me.

**Note**: This explanation is intended to accompany the reading of some example
stage. Reading this text alone, without looking at an example, may confuse you
(it confuses me o_O).

`======` are section separators. It's a line with one or more `=`.

`------` are sub-section separators. A line with one or more `-`.

### First section

The first section are just some variable assignments of the format
`<variable> = <value>;`. Here's a list of the types of values that are used:

* float - A floating point number.
* float_pair - Two comma-separated floating point numbers in parenthesis.
* string - A string.
* lfo - A list of 2-5 comma-separated values in parenthesis that describe an
  LFO, where the first value is a string and the rest are floats. If not all
  values are present, default values are used for the missing ones. Here are
  the list of parameters:
  * waveshape: sin, sqr, saw or tri
  * frequency
  * amplitude
  * phase
  * offset
* lfo_list - A list of comma-separated lfos in braces.

Now a list of variables with its types:

* `gs : float;` Game speed. How fast the obstacles come at you.
* `ps : float;` Player speed. How fast you rotate around the polygon.
* `ch : float_pair;` Color hue. Values in range [0, 1].
* `cs : float_pair;` Color saturation. Values in range [0, 1].
* `cv : float_pair;` Color value. Values in range [0, 1].
* `swp : float;` Color swap time. 0 to disable swap.
* `lfox : lfo_list;` LFOs for expansion.
* `lfoh : lfo_list;` LFOs for hue.
* `lfos : lfo_list;` LFOs for saturation.
* `lfov : lfo_list;` LFOs for value.
* `lfor : lfo_list;` LFOs for rotation.
* `lfoa : lfo_list;` LFOs for tilt angle.
* `lfor : lfo_list;` LFOs for tilt magnitude.
* `st : string;` Soundtrack. Name of file in music info folder.

Notes:
* The types used here are not necessarily the ones used in the game's source
  code.
* The pair of values in the color variables are the two extremes of a gradient
  where the colors for the game are sampled from.
* Color swap time is the time it takes for the colors of the tracks around the
  polygon to swap. It's in milliseconds and can be disabled by using 0.
* Expansion is used to change the size of the polygon and create a speaker-like
  effect.
* Rotation is the angle of rotation of the polygon.

### Second section

The second section contains the walls and wall groups. A wall is an array of
aligned obstacles. Each obstacle in the wall is represented by a floating point
number that is the depth of the obstacle. A zero means there is no obstacle at
that position. In the stage text file, lists of numbers would be too hard to
understand visually, so characters are used to represent different numbers.

The first thing in this section, just like in every section from now on, is a
number in brackets that is the number of different walls there are in the stage.
After this number there is a list of definitions of characters that will be used
to form the walls. Each definition is a comma-separated pair in parenthesis,
where the first element is a character in quotes and the second element is the
floating point number that character represents. The list of definitions
consists in two or more comma-separated definitions in parenthesis. I say two or
more because to make a functional stage you will need at least one character to
represent the number zero and one to represent a non-zero number.

Then there's the list of walls. The list must be surrounded by braces. Each wall
must be in the format `i: c c c c c c;` where `i` is the index of this wall
(the first index must be 0, then 1 and so on) and `c` is one of the characters
you defined. The wall must end with `;`, so, in your definition of characters,
you cannot use the semicolon. Remember that if you have the correct number of
walls (the number at the beginning of the section, in brackets), the last index
must be the number of walls minus 1.

Remember that the number of sides of the polygon can change. For now, the
maximum number of sides is 6, and that's the reason for the six `c`s in my
example, but if one of these walls is used when the polygon is smaller than
that, the last obstacles will not appear, obviously. You can also create walls
with less obstacles, then the remaining obstacles will be filled with zeros.

After closing the braces for the list, there's a sub-section separator. The
following sub-section defines the wall groups, and this will be the same for the
next section to come. Groups are lists of things from which the game can choose
one at random to throw at the player. So, a wall group is just a list of indexes
of walls (the indexes that you just used above) from which the game will choose
one wall at random. If there are two walls in a wall group, each will have a 50%
chance of coming. Let's say you want a group where the chance of the wall `2` is
twice the chance of wall `3`. You can use the list `2, 2, 3`. `3` will have a
1/3 chance and `2` will have 2/3.

Just like in the beginning of the section, the first thing here is the number of
groups. You must count that each single wall you defined will become a group of
itself alone, so the number of groups will be the number of walls plus the
additional number of groups you want to create.

Then comes the list of wall groups, in braces. Each wall group is in the format
`i: [n] e0, e1, e2, ... , en-1;`, where `i` is the index (remember that the
index should start after the last index used in the wall definitions), `n` is
the number of walls in the group and the `e`s are the indexes of each wall in
the group. End the group definition with a `;`.

### Third section

This next section are the gauntlets. Each gauntlet is a sequence of wall groups,
with the spacing between each wall group and the next one. The format of this
section is almost exactly the same as the one before. First there's the number
of gauntlet in brackets. Then there's the unit of spacing between gauntlets in
parenthesis. This unit will be the default value for spacing when you don't
explicitly write one, and when you do, this value will multiply the one you
wrote.

Then there's the list of gauntlets, in braces. The format is almost the same as
the list of wall groups above, except for some added information:
`i: [n] e0|s0|[r0], e1|s1|[r1], ... , en-1|sn-1|[rn-1];` where `i` is the index,
`n` is the number of wall groups, `e#` are indexes of wall groups, `s#` are
floating point numbers, the spacing between this wall group and the next, and
`r#` are integer numbers that represent how many of this wall group will come
in sequence. Both `|s|` and `[r]` are optional in each wall group. Be aware that
if you use `[r]` to repeat a wall group, then the repetitions also count for the
number of elements in the list.

In a gauntlet, each element of the list can have, instead of a wall group index,
a negative number. The meaning of negative numbers is to change the number of
sides of the polygon to the absolute value of that number. This special wall
will exist in the game, but will be invisible. When it reaches the polygon, the
transformation occurs.

After this list, comes a sub-section separator and then the gauntlet groups.
Gauntlet groups are just like wall groups, but with gauntlets instead of walls.
The structure of this section is also exactly the same of the section for wall
groups.

### Last section

This last section defines the sequences. Sequences are basically the general
structure of the stage. You must have a list of sequences for each possible
number of sides that the polygon can have in that stage.

The first thing is the number of different polygons in the stage, in brackets.
Then a pair of comma-separated integer numbers in parenthesis. The first number
is the minimum number of sides of the polygon. Let's say you specify the number
of different polygons to be 2 and the minimum number of sides to be 4. Then your
polygon can be a square and a pentagon only. The second number in parenthesis is
the initial number of sides of the polygon when the stage begins and it's also
the polygon that is shown in the stage select screen.

Now comes the list of lists of sequences. The list must be in braces, like the
others. The format of each entry here `i: [n] list` where `i` is the number of
sides of the polygon, `n` is the number of sequences for this polygon and `list`
is a list in the same format as the ones in the wall groups and gauntlet groups
definition, except that now each number means a gauntlet group.

### Creating your stages

When creating your stages, you should put them in your user folder. The game
creates this folder for you to put you custom stages and music, and also to
save your best times. In gnu/linux systems, this folder will be
~/.config/pascalhexagon. In other systems it should be some user folder that I
don't really know at the moment.

## Music

For music to be played in the game, it needs one text file for music information
and the music file itself. These are in the folders data/music/info and
data/music/audio, respectively. The music info file must contain lines of the
format `<key> = <value>`, where the possible keys are `title`, `artist`,
`album`, `year`, `file` and `markers`. `file` is the name of the music file. If
not present, the game will search for a music file with the same name as the
music info file, with the .ogg extension. `markers` must have a list of numbers
that are points in the music, in milliseconds, where the music can start when
the player retries the stage. When the stage is selected from the stage select
menu, the first marker will be played. If there are no markers in the music info
file, the music always starts at the beginning. The only key that must have a
value is the `title` key.

## The future

Things that I would like to do (or someone else to do) with this project in the
future:

* Implement hyper stages. I have something in mind about that already: it would
be another stage file with the same name of the normal stage, with a -h added.
The stage would become hyper after 60 seconds and then, if you already reached
this time, it becomes available at the select stage menu.

* Create a stage creator/editor.

* Create a install script. It should be able to install in all platforms, but
gnu/linux only would be nice to start with.

* Bundle the game with some nice stages. But stages don't create themselves.

