APP    = pascalhexagon
TARGET = $(APP)

SRC    = src
BUILD  = build
ZENGL  = zengl

MAIN   = $(SRC)/pascalhexagon.pas
SOURCE = $(SRC)/*.pas

FPC    = fpc
FLAGS  = -S2
DEBUG  = -g
STATIC = -dUSE_ZENGL_STATIC

# Operating system and architecture detection
# Far from perfect, but should work for gnu/linux, mac and windows

ifeq ($(OS),Windows_NT)
  OS   = win32
  ARCH = i386
else

  ifeq ($(shell uname -s), Darwin)
    OS = darwin
    ARCH = i386
  else

    ifeq ($(shell uname -s), Linux)
      OS = linux
      ifeq ($(shell uname -m), x86_64)
        ARCH = x86_64
      else
        ARCH = i386
      endif
    endif

  endif

endif

UNITS  = -Fu$(ZENGL)/headers \
         -Fu$(ZENGL)/src \
         -Fu$(ZENGL)/lib/ogg/$(ARCH)-$(OS) \
         -Fu$(ZENGL)/lib/theora/$(ARCH)-$(OS) \
         -Fu$(ZENGL)/lib/zip/$(ARCH)-$(OS) \
         -Fu$(ZENGL)/lib/zlib/$(ARCH)-$(OS)

ifeq ($(OS), win32)
  TARGET = $(APP).exe
  UNITS += -Fu$(ZENGL)/lib/msvcrt/$(ARCH)
endif

$(TARGET): $(SOURCE)
	$(FPC) $(STATIC) $(FLAGS) -FU$(BUILD) $(MAIN) $(UNITS) -o./$(TARGET)

.PHONY: static dynamic debug-static debug-dynamic clean

static: pascalhexagon

dynamic: $(SOURCE)
	$(FPC) $(FLAGS) -FU$(BUILD) $(MAIN) $(UNITS) -o./$(TARGET)

debug debug-static: $(SOURCE)
	$(FPC) $(STATIC) $(DEBUG) $(FLAGS) -FU$(BUILD) $(MAIN) $(UNITS) -o./$(TARGET)

debug-dynamic: $(SOURCE)
	$(FPC) $(DEBUG) $(FLAGS) -FU$(BUILD) $(MAIN) $(UNITS) -o./$(TARGET)

clean:
	rm -f $(BUILD)/*.a $(BUILD)/*.o $(BUILD)/*.ppu $(APP) $(APP).exe log.txt

