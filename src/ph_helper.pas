{ ----------------------------------------------------------------------------
  File: ph_helper.pas

  Unit to implement some generic functions that don't belong anywhere else.
  ----------------------------------------------------------------------------
  Copyright 2016, 2017 Eduardo Mezêncio.

  This file is part of Pascal Hexagon.

  Pascal Hexagon is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Pascal Hexagon is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Pascal Hexagon.  If not, see <http://www.gnu.org/licenses/>.
  ---------------------------------------------------------------------------- }

unit ph_helper;

//=========//
  INTERFACE
//=========//

procedure moveTowards   (var val : single; dest, step : single);
function  limitRange    (val, min, max : single)  : single;
function  simplifyAngle (angle : single)          : single;
function  getColorRGB   (r, g, b : byte)          : longword;
function  getColorHSV   (h, s, v : single)        : longword;

//==============//
  IMPLEMENTATION
//==============//

{ ----------------------------------------------------------------------------
  Procedure to move a value towards other value with given increment.

  val   Value to be changed.
  dest  Target value.
  step  Absolute value of the increment to apply to val.
  ---------------------------------------------------------------------------- }
procedure moveTowards(var val : single; dest, step : single);
begin

	if abs(dest - val) <= step then
		val := dest
	else if val < dest then
		val := val + step
	else
		val := val - step;

end;

{ ----------------------------------------------------------------------------
  Function to limit a value to a given range.

  val     Original value.
  min     Lower bounds of the range.
  max     Upper bounds of the range.

  return  Returns the original value, limited to [min, max].
  ---------------------------------------------------------------------------- }
function limitRange(val, min, max : single) : single;
begin

	if      val < min then limitRange := min
	else if val > max then limitRange := max
	else    limitRange := val;

end;

{ ----------------------------------------------------------------------------
  Function to simplify an angle to a value between 0 and 2*pi.

  angle   The original angle.

  return  Returns the simplified angle.
  ---------------------------------------------------------------------------- }
function simplifyAngle(angle : single) : single;
var
	cycles : integer;
begin

	if angle >= (2 * pi) then
	begin
		cycles := trunc(angle / (2 * pi));
		simplifyAngle := angle - cycles * 2 * pi;
	end
	else if angle < 0 then
	begin
		angle := angle * (-1);
		cycles := trunc(angle / (2 * pi));
		simplifyAngle := 2 * pi - (angle - cycles * 2 * pi);
	end
	else
		simplifyAngle := angle;

end;

{ ----------------------------------------------------------------------------
  Function to get a longword that encodes a color from RGB values.

  r       Byte with the Red value.
  g       Byte with the Green value.
  b       Byte with the Blue value.

  return  Returns the color as a longword.
  ---------------------------------------------------------------------------- }
function getColorRGB(r, g, b: byte) : longword;
begin

	getColorRGB :=   longword(r) * $010000
		           + longword(g) * $000100
		           + longword(b){* $000001};

end;

{ ----------------------------------------------------------------------------
  Function to get a longword that encodes a color from HSV values. Hue is given
  in turns (1 turn = 2*pi) and can be any value. Saturation and Value must be
  between 0 and 1.

  h       Hue value (in turns).
  s       Saturation value (between 0 and 1).
  v       Value value (between 0 and 1).
          ^ yes, I know value value sounds strange

  return  Returns the color as a longword.
  ---------------------------------------------------------------------------- }
function getColorHSV(h, s, v : single) : longword;
var
	c, x, m, r, g, b: single;
	hi: shortint;
begin

	h := h - trunc(h);
	if h < 0 then h := h + 1;
	h := h * 6;
	hi := trunc(h);

	c := v * s;
	x := c * (1 - abs((hi mod 2) + (h - hi) - 1));

	case hi of
		0: begin r:=c; g:=x; b:=0; end;
		1: begin r:=x; g:=c; b:=0; end;
		2: begin r:=0; g:=c; b:=x; end;
		3: begin r:=0; g:=x; b:=c; end;
		4: begin r:=x; g:=0; b:=c; end;
		5: begin r:=c; g:=0; b:=x; end;
	end;

	m := v - c;

	r := r+m; g := g+m; b := b+m;

	getColorHSV := getColorRGB( byte(trunc(r * 255)),
	                            byte(trunc(g * 255)),
	                            byte(trunc(b * 255)) );

end;

end.
