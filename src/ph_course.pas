{ ----------------------------------------------------------------------------
  File: ph_course.pas

  Unit to implement the course structure and functionality. The course is what
  tells the game how to create obstacles for the stage.
  ----------------------------------------------------------------------------
  Copyright 2016, 2017 Eduardo Mezêncio.

  This file is part of Pascal Hexagon.

  Pascal Hexagon is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Pascal Hexagon is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Pascal Hexagon.  If not, see <http://www.gnu.org/licenses/>.
  ---------------------------------------------------------------------------- }

unit ph_course;

//=========//
  INTERFACE
//=========//

uses
	ph_global,
	ph_polygon;

{ An explanation of the elements that are used to compose a course:
  (you can also take a look at the stage file format explanation in README.md)

  Group    : A group of something (e.g. wall group, gauntlet group, etc.) is
             an array of that particular thing, from which the game can choose
             one of the elements of the array at random.

  Obstacle : Each of the individual trapezoids that come towards the player.

  Wall     : An array of aligned obstacles, where each obstacle can have a
             different depth.

  Gauntlet : An ordered list of wall groups, where each wall group can have
             different spacing to the next one. One of the elements of the
             gauntlet can be a change of the number of sides of the polygon,
             instead of a wall group.

  Sequence : An ordered list of gauntlet groups. The course must have a list
             of sequences for each of the different polygons that it uses. }

type
	t_gauntlet_element = record
		wall_group  : smallint;
		space_after : single;
	end;

	t_shortint_array         = array of shortint;
	t_smallint_array         = array of smallint;
	t_gauntlet_element_array = array of t_gauntlet_element;
	p_shortint_array         = ^t_shortint_array;
	p_smallint_array         = ^t_smallint_array;
	p_gauntlet_element_array = ^t_gauntlet_element_array;

	t_wall                   = array[0..(MAX_POLYGON - 1)] of single;
	t_wall_group             = t_smallint_array;
	t_gauntlet               = t_gauntlet_element_array;
	t_gauntlet_group         = t_shortint_array;
	t_sequence               = t_shortint_array;

	t_wall_array             = array of t_wall;
	t_wall_group_array       = array of t_wall_group;
	t_gauntlet_array         = array of t_gauntlet;
	t_gauntlet_group_array   = array of t_gauntlet_group;
	t_sequence_array         = array of t_sequence;
	t_sequence_array_array   = array of t_sequence_array;
	p_wall_array             = ^t_wall_array;
	p_wall_group_array       = ^t_wall_group_array;
	p_gauntlet_array         = ^t_gauntlet_array;
	p_gauntlet_group_array   = ^t_gauntlet_group_array;
	p_sequence_array         = ^t_sequence_array;
	p_sequence_array_array   = ^t_sequence_array_array;

	ph_p_course = ^ph_t_course;
	ph_t_course = record
		min_sides       : shortint;
		max_sides       : shortint;
		init_sides      : shortint;
		walls           : t_wall_array;
		wall_groups     : t_wall_group_array;
		gauntlets       : t_gauntlet_array;
		gauntlet_groups : t_gauntlet_group_array;
		sequences       : t_sequence_array_array;
	end;

	ph_t_slice_array = array[0..(MAX_POLYGON - 1)] of shortint;
	ph_t_depth_array = array[0..(MAX_POLYGON - 1)] of single;


procedure initCourse   (const course : ph_t_course);
function  updateCourse (const course : ph_t_course;
                              dist   : double;
                          var slice  : ph_t_slice_array;
                          var depth  : ph_t_depth_array;
                          var offset : single           ) : shortint;

//==============//
  IMPLEMENTATION
//==============//

var
	distance               : double;
	current_sides          : shortint;
	current_sequence       : shortint;
	current_sequence_index : shortint;
	current_gauntlet       : shortint;
	current_gauntlet_index : shortint;
	next_sequence_sides    : shortint;


function  createNextWall (const course : ph_t_course;
                            var slice  : ph_t_slice_array;
                            var depth  : ph_t_depth_array ) : shortint; forward;
procedure advanceCourse  (const course : ph_t_course); forward;

{ ----------------------------------------------------------------------------
  Procedure to reset the values for the given course.

  course  The current course.
  ---------------------------------------------------------------------------- }
procedure initCourse(const course : ph_t_course);
var
	current_gauntlet_group : shortint;
begin

	with course do
	begin

		distance               := 0;
		current_sides          := init_sides - min_sides;
		current_sequence       := random(length(sequences[current_sides]));
		current_sequence_index := 0;
		current_gauntlet_group := sequences[current_sides]
		                                   [current_sequence]
		                                   [current_sequence_index];
		current_gauntlet       := gauntlet_groups
		                          [current_gauntlet_group]
		                          [random(length(gauntlet_groups[current_gauntlet_group]))];
		current_gauntlet_index := 0;
		next_sequence_sides    := current_sides;

		resetPolygon(init_sides);

	end;

end;

{ ----------------------------------------------------------------------------
  Function to update the current course state. It should be called every frame
  and returns the number of obstacles that must be created in that frame. The
  slice and depth parameters are arrays that will contain information of the
  position and size of each obstacle to be created, while offset will have the
  adjustment needed for the distance of these obstacles.

  course  The current course.
  dist    How much to advance the course.
  slice   Array of slices where obstacles will be created.
  depth   Array of depth of obstacles that will be created.
  offset  Adjustment in distance of slices that will be created.

  return  Returns the number of obstacles to be created.
  ---------------------------------------------------------------------------- }
function updateCourse(const course : ph_t_course;
                            dist   : double;
                        var slice  : ph_t_slice_array;
                        var depth  : ph_t_depth_array;
                        var offset : single           ) : shortint;
begin

	updateCourse := 0;

	distance := distance - dist;
	if distance <= 0 then
	begin
		offset := distance;
		distance := distance + course.gauntlets[current_gauntlet][current_gauntlet_index].space_after;
		updateCourse := createNextWall(course, slice, depth);
		advanceCourse(course);
	end;

end;

{ ----------------------------------------------------------------------------
  Function that sets its arguments to the info about the wall that will be
  created and returns the number of obstacles to create.

  course  The current course.
  slice   Array of slices where obstacles will be created.
  depth   Array of depth of obstacles that will be created.

  return  Returns the number of obstacles to be created.
  ---------------------------------------------------------------------------- }
function createNextWall(const course : ph_t_course;
                          var slice  : ph_t_slice_array;
                          var depth  : ph_t_depth_array ) : shortint;
var
	i, j               : integer;
	current_wall_group : smallint;
	current_wall       : smallint;
begin

	with course do
	begin
		current_wall_group := gauntlets[current_gauntlet]
		                               [current_gauntlet_index].wall_group;

		{ Less than zero means to change the number of sides of the polygon }
		if current_wall_group < 0 then
		begin
			if (-current_wall_group >= min_sides) and
			   (-current_wall_group <= max_sides) then
			begin
				next_sequence_sides := (-current_wall_group) - min_sides;
				createNextWall      := current_wall_group;
			end
			else
				createNextWall      := 0;
		end
		else
		begin
			current_wall := wall_groups[current_wall_group]
			                           [random(length(wall_groups[current_wall_group]))];

			j := 0;
			for i := 0 to (MAX_POLYGON - 1) do
			begin
				if walls[current_wall][i] > 0 then
				begin
					slice[j] := i;
					depth[j] := walls[current_wall][i];
					inc(j);
				end;
			end;

			createNextWall := j;
		end;
	end;

end;

{ ----------------------------------------------------------------------------
  Procedure to advance the course state to the next wall, to be called after
  a wall is created.

  course  The current course.
  ---------------------------------------------------------------------------- }
procedure advanceCourse(const course : ph_t_course);
var
	current_gauntlet_group : shortint;
begin

	with course do
	begin
		inc(current_gauntlet_index);
		if current_gauntlet_index < length(gauntlets[current_gauntlet]) then
			exit;

		{ Current gauntlet walls ended }

		current_gauntlet_index := 0;
		inc(current_sequence_index);
		if   current_sequence_index
		   < length(sequences[current_sides][current_sequence]) then
		begin
			current_gauntlet_group := sequences[current_sides]
			                                   [current_sequence]
			                                   [current_sequence_index];
			current_gauntlet := gauntlet_groups
			                    [current_gauntlet_group]
			                    [random(length(gauntlet_groups[current_gauntlet_group]))];
			exit;
		end;

		{ Current sequence gauntlets ended }

		current_sequence_index := 0;
		current_sides          := next_sequence_sides;
		current_sequence       := random(length(sequences[current_sides]));
		current_gauntlet_group := sequences[current_sides]
		                                   [current_sequence]
		                                   [current_sequence_index];
		current_gauntlet       := gauntlet_groups
		                          [current_gauntlet_group]
		                          [random(length(gauntlet_groups[current_gauntlet_group]))];
	end;

end;

end.
