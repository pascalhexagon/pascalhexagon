{ ----------------------------------------------------------------------------
  File: ph_hud.pas

  Unit that implements functions to draw information over the game screen.
  ----------------------------------------------------------------------------
  Copyright 2016, 2017 Eduardo Mezêncio.

  This file is part of Pascal Hexagon.

  Pascal Hexagon is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Pascal Hexagon is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Pascal Hexagon.  If not, see <http://www.gnu.org/licenses/>.
  ---------------------------------------------------------------------------- }

unit ph_hud;

//=========//
  INTERFACE
//=========//

procedure initHud              ();
procedure updateHudColor       ();

procedure drawTitleHud         ();

procedure updateStageSelectHud ();
procedure drawStageSelectHud   ();

procedure updateGameHudString  ();
procedure drawGameHud          ();

procedure drawGameOverHud      ();

//==============//
  IMPLEMENTATION
//==============//

uses
	math,
	sysutils,
	{$IFDEF USE_ZENGL_STATIC}
	zgl_fx,
	zgl_primitives_2d,
	zgl_render_2d,
	zgl_text,
	{$ELSE}
	zglHeader,
	{$ENDIF}
	ph_global,
	ph_helper,
	ph_resources,
	ph_camera,
	ph_lfo,
	ph_timer,
	ph_stage;

const
	SH_CREDIT_TEXT  = 'A ''CLONE'' OF SUPER HEXAGON BY TERRY CAVANAGH';
	COPYRIGHT_TEXT  = 'COPYLEFT ' + GAME_YEAR + ' ' + GAME_AUTHOR;
	MUSIC_INFO_TIME = 5000;
	MUSIC_INFO_FADE = 1000;

	HUD_MARGIN      = 12;
	HUD_HEIGHT      = 29 + 2 * HUD_MARGIN;
	HUD_TR_WIDTH    = HUD_HEIGHT * 6;
	HUD_TL_WIDTH    = HUD_HEIGHT * 8;
	HUD_BT_WIDTH    = HUD_HEIGHT * 10;
	HUD_ARROW_SIZE  = 24;
	HUD_ALPHA       = 128;

var
	font_str                : string;
	voice_str               : string;
	sfx_str                 : string;
	stage_name_str          : string;
	best_time_str           : string;
	level_str               : string;
	time_str                : string;
	tl_stage_select_display : array[0..5] of TPoint;
	tl_stage_select_arrows  : array[0..5] of TPoint;
	game_tr_display         : array[0..5] of TPoint;
	game_tl_display         : array[0..5] of TPoint;
	color                   : longword;
	color_lfo               : ph_t_lfo;
	scrw, scrh              : integer;
	press_enter_pos         : integer;


procedure drawMusicInfo(alpha : byte); forward;


{ ----------------------------------------------------------------------------
  Procedure to initialize the HUD parameters. It sets the coordinates where to
  draw each display according to the game screen size, so it should be called
  at the game initialization and when the screen size changes.
  ---------------------------------------------------------------------------- }
procedure initHud();
var
	str1, str2 : string;
begin
	scrw := getScreenWidth;
	scrh := getScreenHeight;

	getResourceInfo('font_name'  , str1);
	getResourceInfo('font_author', str2);
	if (str1 <> '') and (str2 <> '')
	                then font_str  := 'FONT ' + str1 + ' BY ' + str2
	                else font_str  := '';
	getResourceInfo('sfx_author' , str1);
	if (str1 <> '') then sfx_str   := 'SFX BY ' + str1
	                else sfx_str   := '';
	getResourceInfo('voice'      , str1);
	if (str1 <> '') then voice_str := 'VOICE BY ' + str1
	                else voice_str := '';

	press_enter_pos := min(floor(scrh * 0.764), scrh - HUD_MARGIN - 160);

	tl_stage_select_display[0].x := scrw / 2 - HUD_BT_WIDTH / 2 + HUD_HEIGHT;
	tl_stage_select_display[0].y := scrh * 3 / 4 - HUD_HEIGHT - HUD_MARGIN / 2;
	tl_stage_select_display[1].x := scrw / 2 + HUD_BT_WIDTH / 2 + HUD_HEIGHT;
	tl_stage_select_display[1].y := scrh * 3 / 4 - HUD_HEIGHT - HUD_MARGIN / 2;
	tl_stage_select_display[2].x := scrw / 2 - HUD_BT_WIDTH / 2 - HUD_HEIGHT;
	tl_stage_select_display[2].y := scrh * 3 / 4 + HUD_HEIGHT + HUD_MARGIN / 2;
	tl_stage_select_display[3].x := tl_stage_select_display[1].x;
	tl_stage_select_display[3].y := tl_stage_select_display[1].y;
	tl_stage_select_display[4].x := tl_stage_select_display[2].x;
	tl_stage_select_display[4].y := tl_stage_select_display[2].y;
	tl_stage_select_display[5].x := scrw / 2 + HUD_BT_WIDTH / 2 - HUD_HEIGHT;
	tl_stage_select_display[5].y := scrh * 3 / 4 + HUD_HEIGHT + HUD_MARGIN / 2;

	tl_stage_select_arrows[0].x := HUD_ARROW_SIZE + scrw * 3 / 4;
	tl_stage_select_arrows[0].y := scrh / 2;
	tl_stage_select_arrows[1].x := HUD_ARROW_SIZE * cos(PI * 2 / 3) + scrw * 3 / 4;
	tl_stage_select_arrows[1].y := HUD_ARROW_SIZE * sin(PI * 2 / 3) + scrh / 2;
	tl_stage_select_arrows[2].x := HUD_ARROW_SIZE * cos(PI * 4 / 3) + scrw * 3 / 4;
	tl_stage_select_arrows[2].y := HUD_ARROW_SIZE * sin(PI * 4 / 3) + scrh / 2;
	tl_stage_select_arrows[3].x := scrw - tl_stage_select_arrows[0].x;
	tl_stage_select_arrows[3].y := tl_stage_select_arrows[0].y;
	tl_stage_select_arrows[4].x := scrw - tl_stage_select_arrows[1].x;
	tl_stage_select_arrows[4].y := tl_stage_select_arrows[1].y;
	tl_stage_select_arrows[5].x := scrw - tl_stage_select_arrows[2].x;
	tl_stage_select_arrows[5].y := tl_stage_select_arrows[2].y;

	game_tr_display[0].x := scrw;
	game_tr_display[0].y := HUD_HEIGHT;
	game_tr_display[1].x := scrw;
	game_tr_display[1].y := 0;
	game_tr_display[2].x := scrw - HUD_TR_WIDTH + HUD_HEIGHT;
	game_tr_display[2].y := HUD_HEIGHT;
	game_tr_display[3].x := game_tr_display[1].x;
	game_tr_display[3].y := game_tr_display[1].y;
	game_tr_display[4].x := game_tr_display[2].x;
	game_tr_display[4].y := game_tr_display[2].y;
	game_tr_display[5].x := scrw - HUD_TR_WIDTH;
	game_tr_display[5].y := 0;

	game_tl_display[0].x := 0;
	game_tl_display[0].y := 0;
	game_tl_display[1].x := 0;
	game_tl_display[1].y := HUD_HEIGHT;
	game_tl_display[2].x := HUD_TL_WIDTH;
	game_tl_display[2].y := 0;
	game_tl_display[3].x := game_tl_display[1].x;
	game_tl_display[3].y := game_tl_display[1].y;
	game_tl_display[4].x := game_tl_display[2].x;
	game_tl_display[4].y := game_tl_display[2].y;
	game_tl_display[5].x := HUD_TL_WIDTH - HUD_HEIGHT;
	game_tl_display[5].y := HUD_HEIGHT;

	color := $FFFFFF;
	initLfo(color_lfo, sin_wave, 1, 0.25, PI / 2, 0.75);

end;

{ ----------------------------------------------------------------------------
  Procedure to update the color of the text using an LFO. Should be called
  every frame.
  ---------------------------------------------------------------------------- }
procedure updateHudColor();
begin

	color := getColorHSV(0, 0, lfoValue(color_lfo));

end;

{ ----------------------------------------------------------------------------
  Procedure to draw text of the title screen.
  ---------------------------------------------------------------------------- }
procedure drawTitleHud();
begin

	{ Title }
	text_draw(getFont(FONT_45), scrw / 2 - 63, scrh * 0.382 - 33,
	          'PASCAL' , TEXT_HALIGN_CENTER or TEXT_VALIGN_CENTER);
	text_draw(getFont(FONT_45), scrw / 2 + 36, scrh * 0.382 + 33,
	          'HEXAGON', TEXT_HALIGN_CENTER or TEXT_VALIGN_CENTER);

	{ Press Enter }
	text_drawEx(getFont(FONT_22), scrw / 2, press_enter_pos,
	            1.0, 0,   'PRESS ENTER'   , 255, color,
	            TEXT_HALIGN_CENTER or TEXT_VALIGN_CENTER);

	{ Super Hexagon credit }
	text_drawEx(getFont(FONT_22), scrw - HUD_MARGIN, scrh - HUD_MARGIN - 96,
	            0.5, 0,   SH_CREDIT_TEXT  , 255, $FFFFFF,
	            TEXT_HALIGN_RIGHT or TEXT_VALIGN_BOTTOM);
	{ Font credit }
	text_drawEx(getFont(FONT_22), scrw - HUD_MARGIN, scrh - HUD_MARGIN - 72,
	            0.5, 0,   font_str        , 255, $FFFFFF,
	            TEXT_HALIGN_RIGHT or TEXT_VALIGN_BOTTOM);
	{ Voice credit }
	text_drawEx(getFont(FONT_22), scrw - HUD_MARGIN, scrh - HUD_MARGIN - 48,
	            0.5, 0,   voice_str       , 255, $FFFFFF,
	            TEXT_HALIGN_RIGHT or TEXT_VALIGN_BOTTOM);
	{ Sfx credit }
	text_drawEx(getFont(FONT_22), scrw - HUD_MARGIN, scrh - HUD_MARGIN - 24,
	            0.5, 0,   sfx_str         , 255, $FFFFFF,
	            TEXT_HALIGN_RIGHT or TEXT_VALIGN_BOTTOM);
	{ Copyright }
	text_drawEx(getFont(FONT_22), scrw - HUD_MARGIN, scrh - HUD_MARGIN,
	            0.5, 0,   COPYRIGHT_TEXT  , 255, $FFFFFF,
	            TEXT_HALIGN_RIGHT or TEXT_VALIGN_BOTTOM);

end;

{ ----------------------------------------------------------------------------
  Procedure to update the text with the current stage information in the stage
  select screen. Should be called every time the currently selected stage
  changes.
  ---------------------------------------------------------------------------- }
procedure updateStageSelectHud();
begin

	if not getStageError then
	begin
		stage_name_str := upperCase(getStageName);
		best_time_str := floatToStrF(getStageBestTime / 1000, ffFixed, 0, 2);
		color := getColorHSV(0, 0, lfoValue(color_lfo));
	end;

end;

{ ----------------------------------------------------------------------------
  Procedure to draw text of the stage select screen.
  ---------------------------------------------------------------------------- }
procedure drawStageSelectHud();
begin

	pr2d_TriList( nil      , @tl_stage_select_display  , nil    ,
	              0        , 5                         , $000000,
	              HUD_ALPHA, PR2D_FILL or FX_BLEND                );

	pr2d_TriList( nil      , @tl_stage_select_arrows   , nil    ,
	              0        , 5                         , color  ,
	              255      , PR2D_FILL or FX_BLEND                );

	text_drawEx(getFont(FONT_22), scrw / 2 + 4, scrh * 0.25 + 4, 1, 0,
	            stage_name_str, 255, $000000,
	            TEXT_HALIGN_CENTER or TEXT_VALIGN_CENTER);
	text_draw(getFont(FONT_22), scrw / 2, scrh * 0.25,
	          stage_name_str , TEXT_HALIGN_CENTER or TEXT_VALIGN_CENTER);

	if getStageError then
		text_draw(getFont(FONT_22), scrw /2, scrh * 3 / 4,
		          'ERROR'      , TEXT_HALIGN_CENTER or TEXT_VALIGN_CENTER)
	else
	begin
		text_draw(getFont(FONT_22), scrw /2, scrh * 3 / 4 - HUD_HEIGHT / 2 + 2,
		          'BEST TIME:' , TEXT_HALIGN_CENTER or TEXT_VALIGN_CENTER);
		text_draw(getFont(FONT_22), scrw /2, scrh * 3 / 4 + HUD_HEIGHT / 2 + 2,
		          best_time_str, TEXT_HALIGN_CENTER or TEXT_VALIGN_CENTER);
	end;

end;

{ ----------------------------------------------------------------------------
  Procedure to update the text of the current level of the player in the main
  game screen. Should be called every time the player level changes.
  ---------------------------------------------------------------------------- }
procedure updateGameHudString();
begin

	readLevelStr(level_str);

end;

{ ----------------------------------------------------------------------------
  Procedure to draw text of the main game screen.
  ---------------------------------------------------------------------------- }
procedure drawGameHud();
var
	t : double;
begin

	pr2d_TriList( nil      , @game_tr_display, nil    ,
	              0        , 5               , $000000,
	              HUD_ALPHA, PR2D_FILL or FX_BLEND      );
	pr2d_TriList( nil      , @game_tl_display, nil    ,
	              0        , 5               , $000000,
	              HUD_ALPHA, PR2D_FILL or FX_BLEND      );

	readTimerStr(time_str);

	text_draw(getFont(FONT_22), scrw - HUD_MARGIN, HUD_MARGIN + 14,
	          time_str , TEXT_HALIGN_RIGHT or TEXT_VALIGN_CENTER);
	text_draw(getFont(FONT_22), HUD_MARGIN, HUD_MARGIN + 14,
	          level_str, TEXT_HALIGN_LEFT  or TEXT_VALIGN_CENTER);

	t := readTimer;
	if t < MUSIC_INFO_TIME then
	begin
		if       t < MUSIC_INFO_FADE then
			drawMusicInfo(byte(floor(t * (255 / MUSIC_INFO_FADE))))
		else if  t < (MUSIC_INFO_TIME - MUSIC_INFO_FADE) then
			drawMusicInfo(255)
		else   { t < MUSIC_INFO_TIME }
			drawMusicInfo(byte(floor((MUSIC_INFO_TIME - t) * (255 / MUSIC_INFO_FADE))));
	end;

end;

{ ----------------------------------------------------------------------------
  Procedure to draw text of the game over screen.
  ---------------------------------------------------------------------------- }
procedure drawGameOverHud();
begin

	text_draw(getFont(FONT_45), scrw / 2 - 33, scrh * 0.382 - 33,
	          'GAME' , TEXT_HALIGN_CENTER or TEXT_VALIGN_CENTER);
	text_draw(getFont(FONT_45), scrw / 2 + 33, scrh * 0.382 + 33,
	          'OVER' , TEXT_HALIGN_CENTER or TEXT_VALIGN_CENTER);

	if getNewRecord then
		text_drawEx(getFont(FONT_22), scrw / 2, scrh * 0.628, 1, 0,
		            'NEW RECORD', 255, color,
		            TEXT_HALIGN_CENTER or TEXT_VALIGN_CENTER);

end;

procedure drawMusicInfo(alpha : byte);
begin

	text_drawEx(getFont(FONT_22), HUD_MARGIN, scrh - HUD_MARGIN - 64, 1.0, 0,
	            getStageMusicString(STAGE_MUSIC_INFO_TITLE)^     , alpha, $FFFFFF,
	            TEXT_HALIGN_LEFT or TEXT_VALIGN_BOTTOM);
	text_drawEx(getFont(FONT_22), HUD_MARGIN, scrh - HUD_MARGIN - 32, 0.6, 0,
	            getStageMusicString(STAGE_MUSIC_INFO_ARTIST)^    , alpha, $FFFFFF,
	            TEXT_HALIGN_LEFT or TEXT_VALIGN_BOTTOM);
	text_drawEx(getFont(FONT_22), HUD_MARGIN, scrh - HUD_MARGIN     , 0.6, 0,
	            getStageMusicString(STAGE_MUSIC_INFO_ALBUM_YEAR)^, alpha, $FFFFFF,
	            TEXT_HALIGN_LEFT or TEXT_VALIGN_BOTTOM);

end;

end.
