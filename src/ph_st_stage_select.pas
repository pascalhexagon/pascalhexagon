{ ----------------------------------------------------------------------------
  File: ph_st_stage_select.pas

  Unit for the stage select state. The interface contains only the procedure to
  activate this state. This procedure changes the update and draw callbacks to
  the ones implemented in this file.
  ----------------------------------------------------------------------------
  Copyright 2016, 2017 Eduardo Mezêncio.

  This file is part of Pascal Hexagon.

  Pascal Hexagon is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Pascal Hexagon is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Pascal Hexagon.  If not, see <http://www.gnu.org/licenses/>.
  ---------------------------------------------------------------------------- }

unit ph_st_stage_select;

//=========//
  INTERFACE
//=========//

procedure loadStageSelect();

//==============//
  IMPLEMENTATION
//==============//

uses
	{$IFDEF USE_ZENGL_STATIC}
	zgl_keyboard,
	zgl_main,
	zgl_render_2d,
	{$ELSE}
	zglHeader,
	{$ENDIF}
	ph_global,
	ph_helper,
	ph_resources,
	ph_colors,
	ph_camera,
	ph_polygon,
	ph_stage,
	ph_hud,
	ph_st_game;

var
	current : smallint;
	ready   : boolean;
	angle   : single;


procedure init   ();            forward;
procedure update (dt : double); forward;
procedure draw   ();            forward;

{ ----------------------------------------------------------------------------
  Procedure to set the state to the stage select state, changing the callback
  functions update and draw accordingly.
  ---------------------------------------------------------------------------- }
procedure loadStageSelect();
begin

	zgl_reg(SYS_UPDATE, @update);
	zgl_reg(SYS_DRAW  , @draw  );

	init;

end;

{ ----------------------------------------------------------------------------
  Procedure to initialize the stage select state.
  ---------------------------------------------------------------------------- }
procedure init();
begin

	angle   := 0;
	ready   := true;
	current := getCurrentStageIndex;

	setColorSwap(false);

	resetCamera;
	setTiltAngle(0);
	setTiltMagnitude(0);
	calculateTilt;
	resetPolygon(getStageInitSides);
	setPolygonAnimPause(false);

	updateStageSelectHud;

end;

{ ----------------------------------------------------------------------------
  Procedure to be used as update callback for the stage select state.

  dt  Time elapsed since last frame (in milliseconds).
  ---------------------------------------------------------------------------- }
procedure update(dt : double);
var
	changed    : boolean = false;
	lfo_val_h,
	lfo_val_s,
	lfo_val_v  : single;
begin

	if key_press ( K_ESCAPE ) then zgl_exit;

	{ Accept commands only if not in stage change animation. }
	if ready then
	begin
		if      key_down ( K_LEFT  ) then
		begin
			current := (current + getStagesCount - 1) mod getStagesCount;
			setRotationSpeed(1000 / (POLYGON_ANIM_DURATION * 1.05));
			changed := true;
		end
		else if key_down ( K_RIGHT ) then
		begin
			current := (current + 1) mod getStagesCount;
			setRotationSpeed(-1000 / (POLYGON_ANIM_DURATION * 1.05));
			changed := true;
		end
		else if key_press ( K_ENTER ) then
		begin
			if not getStageError then
			begin
				loadGame;
				exit;
			end;
		end;

		{ Start the rotation animation }
		if changed then
		begin
			playSfx(SFX_ROTATE);
			setCurrentStage(current);
			angle := 360 / getStageInitSides;
			setRotationSpeed(getRotationSpeed * angle);
			resetStageLfosPhase;
			updateStageSelectHud;
			changePolygon(getStageInitSides);
			ready := false;
		end;
	end
	else
	begin
		{ Do the rotation animation }
		rotateCamera(dt);

		if ((getRotationSpeed > 0) and
		    (getCameraAngle > angle )) or
		   ((getRotationSpeed < 0) and
		    (getCameraAngle < 360 - angle)) then
		begin
			setCameraAngle(0);
			swapColors;
			ready := true;
		end;

		calculateTilt;
	end;

	key_clearState;

	updateHudColor;

	lfo_val_h := getStageLfoMix(STAGE_LFO_HUE);
	lfo_val_s := getStageLfoMix(STAGE_LFO_SAT);
	lfo_val_v := getStageLfoMix(STAGE_LFO_VAL);

	setGameColors( getStageColor(STAGE_COLOR_H0) + lfo_val_h,
	               getStageColor(STAGE_COLOR_H1) + lfo_val_h,
	               limitRange(getStageColor(STAGE_COLOR_S0) + lfo_val_s, 0, 1),
	               limitRange(getStageColor(STAGE_COLOR_S1) + lfo_val_s, 0, 1),
	               limitRange(getStageColor(STAGE_COLOR_V0) + lfo_val_v, 0, 1),
	               limitRange(getStageColor(STAGE_COLOR_V1) + lfo_val_v, 0, 1)  );

	updatePolygon(dt);

end;

{ ----------------------------------------------------------------------------
  Procedure to be used as draw callback for the stage select state.
  ---------------------------------------------------------------------------- }
procedure draw();
begin

	batch2d_begin;

	useCamera;
	drawPolygon;

	useNoCamera;
	drawStageSelectHud;

	batch2d_end;

end;

end.
