{ ----------------------------------------------------------------------------
  File: ph_file.pas

  Unit that implements an interface to handle the reading of text files. To be
  used by the stage parser.
  ----------------------------------------------------------------------------
  Copyright 2016, 2017 Eduardo Mezêncio.

  This file is part of Pascal Hexagon.

  Pascal Hexagon is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Pascal Hexagon is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Pascal Hexagon.  If not, see <http://www.gnu.org/licenses/>.
  ---------------------------------------------------------------------------- }

unit ph_file;

//=========//
  INTERFACE
//=========//

procedure openPhFile             (const fname : string);
procedure resetPhFile            ();
procedure closePhFile            ();

procedure skipWhitespace         ();
procedure readChar               (skip_white : boolean = true);
procedure readName               (skip_white : boolean = true);
procedure readInteger            (skip_white : boolean = true);
procedure readFloat              (skip_white : boolean = true);
procedure readString             (skip_white : boolean = true);

function  getFileChar            () : char;
function  getFileString          () : string;
function  getFileInteger         () : integer;
function  getFileFloat           () : real;
function  getFileName            () : string;
function  getLine                () : smallint;

function  fileErrorMessage       () : string;

function  charIsWhitespace       () : boolean;
function  charIsLetter           () : boolean;
function  charIsValidPunctuation () : boolean;
function  charIsNumber           (minus : boolean = true;
                                  dot   : boolean = false) : boolean;
function  stringIsNumber         () : boolean;

function  phEof                  () : boolean;

//==============//
  IMPLEMENTATION
//==============//

uses
	sysutils;

var
	c : char;
	s : string;
	f : textFile;

	filename  : string;
	line, pos : smallint;


procedure phRead(); forward;


{ ----------------------------------------------------------------------------
  Procedure to open a file for reading.

  fname  Name of the file.
  ---------------------------------------------------------------------------- }
procedure openPhFile(const fname : string);
begin

	assignFile(f, fname);
	filename := fname;

end;

{ ----------------------------------------------------------------------------
  Procedure to reset the file being read.
  ---------------------------------------------------------------------------- }
procedure resetPhFile();
begin

	reset(f);
	c    := char(0);
	line := 1;
	pos  := 1;

end;

{ ----------------------------------------------------------------------------
  Procedure to close the file.
  ---------------------------------------------------------------------------- }
procedure closePhFile();
begin

	closeFile(f);

end;

{ ----------------------------------------------------------------------------
  Procedure to skip all whitespaces until a non-whitespace character appears.
  ---------------------------------------------------------------------------- }
procedure skipWhitespace();
begin

	while charIsWhitespace do
		phRead;

end;

{ ----------------------------------------------------------------------------
  Procedure to read a single character.

  skip_white  Whether to skip leading whitespace characters.
  ---------------------------------------------------------------------------- }
procedure readChar(skip_white : boolean);
begin

	if not charIsWhitespace then phRead;
	if skip_white then skipWhitespace;

end;

{ ----------------------------------------------------------------------------
  Procedure to read a name that consists of a letter followed by alphanumeric
  characters.

  skip_white  Whether to skip leading whitespace characters.
  ---------------------------------------------------------------------------- }
procedure readName(skip_white : boolean);
var
	first : boolean = true;
begin

	if skip_white then skipWhitespace;

	s := '';
	if charIsLetter then
	begin
		s := s + c;
		first := false;
	end;

	while true do
	begin
		phRead;

		if first then
		begin
			if not charIsLetter then break;
			first := false;
		end
		else
			if not (charIsLetter or
			        charIsNumber or
			        charIsValidPunctuation) then
				break;

		s := s + c;
	end;

end;

{ ----------------------------------------------------------------------------
  Procedure to read an integer number.

  skip_white  Whether to skip leading whitespace characters.
  ---------------------------------------------------------------------------- }
procedure readInteger(skip_white : boolean);
begin

	if skip_white then skipWhitespace;

	s := '';
	if charIsNumber then s := s + c;

	while true do
	begin
		phRead;
		if not charIsNumber(false) then break;
		s := s + c;
	end;

end;

{ ----------------------------------------------------------------------------
  Procedure to read a floating point number.

  skip_white  Whether to skip leading whitespace characters.
  ---------------------------------------------------------------------------- }
procedure readFloat(skip_white : boolean);
var
	point : boolean = false;
begin

	if skip_white then skipWhitespace;

	s := '';
	if charIsNumber(true, true) then
	begin
		if c = '.' then point := true;
		s := s + c;
	end;

	while true do
	begin
		phRead;

		if c = '.' then
		begin
			if point then break
			else point := true;
		end
		else if not charIsNumber(false) then break;

		s := s + c;
	end;

end;

{ ----------------------------------------------------------------------------
  Procedure to read a string delimited by single or double quotes.

  skip_white  Whether to skip leading whitespace characters.
  ---------------------------------------------------------------------------- }
procedure readString(skip_white : boolean);
var
	delimiter : char;
begin

	if skip_white then skipWhitespace;

	if (c = '''') or (c = '"') then
		delimiter := c
	else
		delimiter := ' ';

	s := '';
	phRead;

	while ((delimiter = ' ') and (charIsWhitespace))
	      or (c = delimiter) or (c = char(10)) or eof(f) do
	begin
		s := s + c;
		phRead;
	end;

end;

{ ----------------------------------------------------------------------------
  Function to return the last character read.

  return  Returns the last character read.
  ---------------------------------------------------------------------------- }
function getFileChar() : char;
begin

	getFileChar := c;

end;

{ ----------------------------------------------------------------------------
  Function to return the last string read.

  return  Returns the last string read.
  ---------------------------------------------------------------------------- }
function getFileString() : string;
begin

	getFileString := s;

end;

{ ----------------------------------------------------------------------------
  Function to return the last string read as an integer number.

  return  Returns the last integer number read.
  ---------------------------------------------------------------------------- }
function getFileInteger() : integer;
begin

	getFileInteger := strToInt(s);

end;

{ ----------------------------------------------------------------------------
  Function to return the last string read as a floating point number.

  return  Returns the last floating point number read.
  ---------------------------------------------------------------------------- }
function getFileFloat() : real;
begin

	getFileFloat := strToFloat(s);

end;

{ ----------------------------------------------------------------------------
  Function to return the name of the file being read.

  return  Returns the name of the file being read.
  ---------------------------------------------------------------------------- }
function getFileName() : string;
begin

	getFileName := filename;

end;

{ ----------------------------------------------------------------------------
  Function to return the number of the current line.

  return  Returns the current line number.
  ---------------------------------------------------------------------------- }
function getLine() : smallint;
begin

	getLine := line;

end;

{ ----------------------------------------------------------------------------
  Function to return a formatted error message with the position where the
  error ocurred.

  return  Returns a string with the formatted error message.
  ---------------------------------------------------------------------------- }
function fileErrorMessage() : string;
begin

	fileErrorMessage :=   'Error in file ' + filename
	                    + ', line '        + intToStr(line)
	                    + ', pos '         + intToStr(pos)  + ': ';

end;

{ ----------------------------------------------------------------------------
  Function to check if the last character read is a whitespace character.

  return  Returns whether the last character is a whitespace character.
  ---------------------------------------------------------------------------- }
function charIsWhitespace() : boolean;
begin

	if    (c = ' ')
	   or (c = char(9 )) // \t
	   or (c = char(10)) // \n
	   or (c = char(13)) // \r
	then
		charIsWhitespace := true
	else
		charIsWhitespace := false;

end;

{ ----------------------------------------------------------------------------
  Function to check if the last character read is a letter.

  return  Returns whether the last character is a letter.
  ---------------------------------------------------------------------------- }
function charIsLetter() : boolean;
begin

	if    ( (c > char(64)) and (c <= char( 90)) )
	   or ( (c > char(96)) and (c <= char(122)) )
	then
		charIsLetter := true
	else
		charIsLetter := false;

end;

{ ----------------------------------------------------------------------------
  Function to check if the last character read is valid punctuation. The valid
  punctuation for this parser is '_', '-' or '.'.

  return  Returns whether the last character is a punctuation character.
  ---------------------------------------------------------------------------- }
function charIsValidPunctuation() : boolean;
begin

	if (c = '_') or (c = '-') or (c = '.') then
		charIsValidPunctuation := true
	else
		charIsValidPunctuation := false;

end;

{ ----------------------------------------------------------------------------
  Function to check if the last character read is part of a number.

  minus   Whether to accept '-' as part of a number.
  dot     Whether to accept '.' as part of a number.

  return  Returns whether the last character is part of a number.
  ---------------------------------------------------------------------------- }
function charIsNumber(minus : boolean; dot : boolean) : boolean;
begin

	if c = '-' then
	begin
		if minus then charIsNumber := true
		else charIsNumber := false;
	end
	else if c = '.' then
	begin
		if dot then charIsNumber := true
		else charIsNumber := false;
	end
	else
	begin
		if (c >= char(48)) and (c < char(58))
		then
			charIsNumber := true
		else
			charIsNumber := false;
	end;

end;

{ ----------------------------------------------------------------------------
  Function to check if the last string read is a number. To be used only after
  calling readInteger or readFloat.

  return  Returns whether the last string read is a number.
  ---------------------------------------------------------------------------- }
function stringIsNumber() : boolean;
var
	last_char : char;
begin

	if length(s) = 0 then
		stringIsNumber := false
	else
	begin
		last_char := s[length(s)];
		if (last_char >= char(48)) and (last_char < char(58))
		then
			stringIsNumber := true
		else
			stringIsNumber := false;
	end;

end;

{ ----------------------------------------------------------------------------
  Function to check for EOF.

  return  Returns whether EOF was reached.
  ---------------------------------------------------------------------------- }
function phEof() : boolean;
begin

	phEof := eof(f);

end;

{ ----------------------------------------------------------------------------
  Procedure to read a character. All reading operations in this unit must be
  done with this procedure, so the position is updated acccordingly to report
  errors correctly.
  ---------------------------------------------------------------------------- }
procedure phRead();
begin

	read(f, c);
	if c = char(10) then
	begin
		inc(line);
		pos := 1;
	end
	else
		inc(pos);

end;

end.
