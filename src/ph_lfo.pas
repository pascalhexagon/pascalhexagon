{ ----------------------------------------------------------------------------
  File: ph_lfo.pas

  Unit that implements a type for LFOs and operations to use them.
  ----------------------------------------------------------------------------
  Copyright 2016, 2017 Eduardo Mezêncio.

  This file is part of Pascal Hexagon.

  Pascal Hexagon is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Pascal Hexagon is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Pascal Hexagon.  If not, see <http://www.gnu.org/licenses/>.
  ---------------------------------------------------------------------------- }

unit ph_lfo;

//=========//
  INTERFACE
//=========//

type
	ph_t_wave = (sin_wave, sqr_wave, saw_wave, tri_wave, no_wave);

	ph_p_lfo = ^ph_t_lfo;
	{ ------------------------------------------------------------------------
	  Record that represents an LFO. The LFO needs to be initialized with a
	  wave shape, frequency, amplitude, phase, offset and the time it was
	  initialized. After that you can get values from it with lfoValue.
	  ------------------------------------------------------------------------ }
	ph_t_lfo = record
		wave       : ph_t_wave; {< Wave shape. }
		frequency  : single;    {< Frequency (in Hz). }
		amplitude  : single;    {< Amplitude (multiplies the values). }
		phase      : single;    {< Phase correction. }
		offset     : single;    {< Offset (adds to values). }
		start_time : tDateTime; {< Reference time to calculate values from. }
	end;

procedure initLfo       (var lfo   : ph_t_lfo;
                             wave  : ph_t_wave;
                             f     : single;
                             a     : single = 1;
                             p     : single = 0;
                             o     : single = 0);

procedure resetLfoPhase (var lfo   : ph_t_lfo);
function  lfoValue      (var lfo   : ph_t_lfo)  : single;
function  lfoMix        (    lfo   : ph_p_lfo;
                             count : shortint ) : single;

//==============//
  IMPLEMENTATION
//==============//

uses
	sysutils,
	dateutils;

{ ----------------------------------------------------------------------------
  Procedure to initialize an LFO.

  lfo   LFO to be initialized.
  wave  Waveform of the LFO.
  f     Frequency of the LFO (in Hz).
  a     Amplitude of the LFO.
  p     Phase correction of the LFO (in radians).
  o     Offset of the LFO.
  ---------------------------------------------------------------------------- }
procedure initLfo(var lfo  : ph_t_lfo;
                      wave : ph_t_wave;
                      f    : single;
                      a    : single;
                      p    : single;
                      o    : single);
begin

	lfo.wave       := wave;
	lfo.frequency  := f;
	lfo.amplitude  := a;
	lfo.phase      := p;
	lfo.offset     := o;
	lfo.start_time := now;

end;

{ ----------------------------------------------------------------------------
  Procedure to reset the phase of an LFO to its phase value.

  lfo  The LFO.
  ---------------------------------------------------------------------------- }
procedure resetLfoPhase (var lfo: ph_t_lfo);
begin

	lfo.start_time := now;

end;

{ ----------------------------------------------------------------------------
  Function to get the current value from an LFO.

  lfo     The LFO.

  return  Returns the current value of the LFO.
  ---------------------------------------------------------------------------- }
function lfoValue(var lfo: ph_t_lfo) : single;
var
	t, val : single;
begin

	with lfo do
	begin

		t := millisecondsBetween(now, start_time) / 1000;

		case wave of

			sin_wave: {  .-.     .-.     .-.     .-.
			            /   \   /   \   /   \   /   \
			                 `-´     `-´     `-´     `-´ }
			begin

				val := amplitude * sin(2 * pi * frequency * t + phase);

			end;

			sqr_wave: { ___     ___     ___     ___
			               |   |   |   |   |   |   |   |
			               |___|   |___|   |___|   |___| }
			begin

				if sin(2 * pi * frequency * t + phase) > 0 then
					val :=  amplitude
				else
					val := -amplitude;

			end;

			saw_wave: {   /|  /|  /|  /|  /|  /|  /|  /|
			             / | / | / | / | / | / | / | / |
			            /  |/  |/  |/  |/  |/  |/  |/  | }
			begin

				val := t * frequency + phase / (2 * pi);
				val := (2 * (val - trunc(0.5 + val))) * amplitude;

			end;

			tri_wave: {   /\    /\    /\    /\    /\
			             /  \  /  \  /  \  /  \  /  \  /
			            /    \/    \/    \/    \/    \/  }
			begin

				val := t * frequency + phase / (2 * pi) + 0.25;
				val := (2 * (val - trunc(0.5 + val)));
				if val > 0 then
					val := -val;
				val := (val + 0.5) * 2 * amplitude;

			end;

			no_wave: val := 0;

		end;

		lfoValue := val + offset;

	end;

end;

{ ----------------------------------------------------------------------------
  Function to get the mix (sum) of the values from an array of LFOs.

  lfo     Pointer to the first LFO of the array.
  count   Number of LFOs in the array.

  return  Returns the mix (sum) of the values from LFOs of the array.
  ---------------------------------------------------------------------------- }
function lfoMix( lfo   : ph_p_lfo;
                 count : shortint  ) : single;
var
	i   : shortint;
	sum : single = 0;
begin

	for i := 0 to (count - 1) do
		sum := sum + lfoValue(lfo[i]);

	lfoMix := sum;

end;

end.
