{ ----------------------------------------------------------------------------
  File: ph_resources.pas

  Unit that manages and provides ways to access the resources used by the game.
  Resources are fonts, sound effects and music.
  ----------------------------------------------------------------------------
  Copyright 2016, 2017 Eduardo Mezêncio.

  This file is part of Pascal Hexagon.

  Pascal Hexagon is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Pascal Hexagon is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Pascal Hexagon.  If not, see <http://www.gnu.org/licenses/>.
  ---------------------------------------------------------------------------- }

unit ph_resources;

//=========//
  INTERFACE
//=========//

uses
	classes,
	ph_global;

const
	FONT_22         = 0;
	FONT_45         = 1;
	FONTS_COUNT     = 2;

	VOICE_PH        = 0;
	VOICE_BEGIN     = 1;
	VOICE_EXCELLENT = 2;
	VOICE_LINE      = 3;
	VOICE_TRIANGLE  = 4;
	VOICE_SQUARE    = 5;
	VOICE_PENTAGON  = 6;
	VOICE_HEXAGON   = 7;
	VOICE_GAME_OVER = 8;  {< 8, 9, 10, 11, 12 are all game over sounds }
	SFX_START       = 13;
	SFX_ROTATE      = 14;
	SFX_BEGIN       = 15;
	SFX_COLLISION   = 16;
	SFX_NEW_RECORD  = 17;
	SOUNDS_COUNT    = 18;

	MAX_MARKERS     = 8;

	VOICE_VOLUME    = 1;
	SFX_VOLUME      = 0.8;
	MUSIC_VOLUME    = 0.5;

	DEFAULT_MUSIC_EXTENSION = '.ogg';

type
	{ ------------------------------------------------------------------------
	  Record to store information about a music track.

	  The member info holds a list of strings in the format "key=value" where
	  the expected keys are: "file", "title", "artist", "album", "year". The
	  record for a track must have at least a value for the "title" key.

	  Markers are the positions, in milliseconds, where the track can be when
	  the player retries a stage after game over.
	  ------------------------------------------------------------------------ }
	ph_t_music_info = record
		name         : string;      {< Name of the music info file. }
		user         : boolean;     {< Whether the track is from the user folder. }
		error        : boolean;     {< Whether there was an error loading this track. }
		info         : TStringList; {< List of strings with track information. }
		markers      : array[0..(MAX_MARKERS - 1)] of double; {< Array of markers. }
		marker_count : shortint;    {< Number of markers. }
	end;

procedure getResourceInfo      (const k : string;
                                  out v : string   );
procedure getMusicInfo         (      m : smallint;
                                const k : string;
                                  out v : string   );
function  getMusicIndex        (const name : string) : smallint;
function  getMusicFirstMarker  (m : smallint)        : double;
function  getMusicRandomMarker (m : smallint)        : double;
function  getFont              (f : shortint)        : PFont;
function  getSound             (s : shortint)        : PSound;

procedure loadResourcesInfo    ();
procedure loadFonts            ();
procedure loadSounds           ();
procedure loadMusicInfo        ();

procedure freeResources        ();

procedure playVoice            (v : shortint);
procedure playSfx              (s : shortint);
procedure playMusic            (m : smallint);
procedure stopMusic            ();
procedure seekMusic            (ms : double);

//==============//
  IMPLEMENTATION
//==============//

uses
	sysutils,
	{$IFDEF USE_ZENGL_STATIC}
	zgl_font,
	zgl_log,
	zgl_sound,
	zgl_sound_ogg,
	zgl_textures_tga,
	{$ELSE}
	zglHeader,
	{$ENDIF}
	ph_user_data;

var
	fonts      : array [0..(FONTS_COUNT  - 1)] of PFont;
	sounds     : array [0..(SOUNDS_COUNT - 1)] of PSound;
	music      : array of ph_t_music_info;
	music_id   : integer = 0;
	info       : TStringList;
	music_tags : TStringList;


procedure initMusicTags         (); forward;
procedure errorLoadingMusicInfo (const error_string : string;
                                 const file_name    : string;
                                       line_number  : integer); forward;
procedure loadMusicInfoFromFile (const fname : string;
                                   var m     : ph_t_music_info); forward;


{ ----------------------------------------------------------------------------
  Procedure to get a string from the info string list. It changes all letters
  to upper case.

  k       Key of the desired value in the string list.
  v       String where to store the value of the given key.
  ---------------------------------------------------------------------------- }
procedure getResourceInfo(const k : string;
                            out v : string );
begin

	v := upperCase(info.values[k]);

end;

{ ----------------------------------------------------------------------------
  Procedure to get a string from the music info string list. It changes all
  letters to upper case.

  m       Index of the music.
  k       Key of the desired value in the string list.
  v       String where to store the value of the given key.
  ---------------------------------------------------------------------------- }
procedure getMusicInfo(      m : smallint;
                       const k : string;
                         out v : string   );
begin

	v := upperCase(music[m].info.values[k]);

end;

{ ----------------------------------------------------------------------------
  Function to get the index of the music with a given name, if there is one.

  name    Name of the music info file.

  return  Returns the index of the music, or -1 if it does not exists.
  ---------------------------------------------------------------------------- }
function getMusicIndex(const name : string) : smallint;
var
	i : smallint;
begin

	for i := 0 to (length(music) - 1) do
	begin
		if (music[i].name  = name ) and
		   (music[i].error = false) then
		begin
			getMusicIndex := i;
			exit;
		end;
	end;

	getMusicIndex := -1;

end;

{ ----------------------------------------------------------------------------
  Function to get the first marker of a music track. Used when entering a
  stage, to play the track from the beginning.

  m       Index of the music track.

  return  Returns the first marker (in milliseconds).
  ---------------------------------------------------------------------------- }
function getMusicFirstMarker(m : smallint) : double;
begin

	if m < 0 then exit(0);

	getMusicFirstMarker := music[m].markers[0];

end;

{ ----------------------------------------------------------------------------
  Function to get a random marker of a music track. Used when retrying a stage,
  to play the track from a random position.

  m       Index of the music track.

  return  Returns a random marker (in milliseconds).
  ---------------------------------------------------------------------------- }
function getMusicRandomMarker(m : smallint) : double;
begin

	if m < 0 then exit(0);

	getMusicRandomMarker := music[m].markers[random(music[m].marker_count)];

end;

{ ----------------------------------------------------------------------------
  Function to get a pointer to a font.

  f       Index of the font.

  return  Returns a pointer to the font.
  ---------------------------------------------------------------------------- }
function getFont(f : shortint) : PFont;
begin

	getFont := fonts[f];

end;

{ ----------------------------------------------------------------------------
  Function to get a pointer to a sound effect.

  s       Index of the sound effect.

  return  Returns a pointer to the sound effect.
  ---------------------------------------------------------------------------- }
function getSound(s : shortint) : PSound;
begin

	if s = VOICE_GAME_OVER then
		getSound := sounds[s + random(5)]
	else
		getSound := sounds[s];

end;

{ ----------------------------------------------------------------------------
  Procedure to load information on the resources used by the game.
  ---------------------------------------------------------------------------- }
procedure loadResourcesInfo();
var
	f        : text;
	fname    : string;
	str      : string;
	k, v     : string;
	char_pos : integer;
begin

	info := TStringList.create;

	fname := DATA_DIR + 'data_info';
	if not fileExists(fname) then exit;

	assign(f, fname);
	reset(f);

	while not eof(f) do
	begin

		{ Read a line, ignoring lines that are not key=value pairs }
		readln(f, str);
		char_pos := pos('=', str);
		if char_pos = 0 then continue;

		k := trim(copy(str, 1, char_pos - 1));
		v := trim(copy(str, char_pos + 1, 255));

		info.values[k] := v;

	end;

	close(f);

end;

{ ----------------------------------------------------------------------------
  Procedure to load the font used by the game.
  ---------------------------------------------------------------------------- }
procedure loadFonts();
begin

	fonts[FONT_22] := font_loadFromFile(DATA_DIR + 'biu22.zfi');
	fonts[FONT_45] := font_loadFromFile(DATA_DIR + 'biu45.zfi');

end;

{ ----------------------------------------------------------------------------
  Procedure to load all sound effects used by the game.
  ---------------------------------------------------------------------------- }
procedure loadSounds();
begin

	sounds[VOICE_PH]            := snd_loadFromFile(DATA_DIR + 'ph.ogg'            , 1);
	sounds[VOICE_BEGIN]         := snd_loadFromFile(DATA_DIR + 'begin.ogg'         , 1);
	sounds[VOICE_EXCELLENT]     := snd_loadFromFile(DATA_DIR + 'excellent.ogg'     , 1);
	sounds[VOICE_LINE]          := snd_loadFromFile(DATA_DIR + 'line.ogg'          , 1);
	sounds[VOICE_TRIANGLE]      := snd_loadFromFile(DATA_DIR + 'triangle.ogg'      , 1);
	sounds[VOICE_SQUARE]        := snd_loadFromFile(DATA_DIR + 'square.ogg'        , 1);
	sounds[VOICE_PENTAGON]      := snd_loadFromFile(DATA_DIR + 'pentagon.ogg'      , 1);
	sounds[VOICE_HEXAGON]       := snd_loadFromFile(DATA_DIR + 'hexagon.ogg'       , 1);
	sounds[VOICE_GAME_OVER]     := snd_loadFromFile(DATA_DIR + 'game_over_1.ogg'   , 1);
	sounds[VOICE_GAME_OVER + 1] := snd_loadFromFile(DATA_DIR + 'game_over_2.ogg'   , 1);
	sounds[VOICE_GAME_OVER + 2] := snd_loadFromFile(DATA_DIR + 'game_over_3.ogg'   , 1);
	sounds[VOICE_GAME_OVER + 3] := snd_loadFromFile(DATA_DIR + 'game_over_4.ogg'   , 1);
	sounds[VOICE_GAME_OVER + 4] := snd_loadFromFile(DATA_DIR + 'game_over_5.ogg'   , 1);
	sounds[SFX_START]           := snd_loadFromFile(DATA_DIR + 'sfx_start.ogg'     , 1);
	sounds[SFX_ROTATE]          := snd_loadFromFile(DATA_DIR + 'sfx_rotate.ogg'    , 3);
	sounds[SFX_BEGIN]           := snd_loadFromFile(DATA_DIR + 'sfx_begin.ogg'     , 1);
	sounds[SFX_COLLISION]       := snd_loadFromFile(DATA_DIR + 'sfx_collision.ogg' , 1);
	sounds[SFX_NEW_RECORD]      := snd_loadFromFile(DATA_DIR + 'sfx_new_record.ogg', 1);

end;

{ ----------------------------------------------------------------------------
  Procedure to load all music info from system and user folders.
  ---------------------------------------------------------------------------- }
procedure loadMusicInfo();
var
	search  : TSearchRec;
	found   : TStringList;
	path    : string;
	i       : integer;
	m, n, v : string;
begin

	initMusicTags;

	found := TStringList.create;
	found.sorted := true;
	found.add( '.=ignore');
	found.add('..=ignore');

	{ findFirst returns 0 if at least one file is found }
	if findFirst(MUSIC_INFO_DIR + '*', faAnyFile, search) = 0 then
	begin
		repeat
			if found.values[search.name] <> 'ignore' then
			   found.values[search.name] := 'sys';
		until findNext(search) <> 0;
	end;
	findClose(search);

	{ User folder is searched after system folder so that if a file is found in
	both locations, the one in the user folder takes priority }
	if findFirst(getUserMusicInfoDir + '*', faAnyFile, search) = 0 then
	begin
		repeat
			if found.values[search.name] <> 'ignore' then
			   found.values[search.name] := 'usr';
		until findNext(search) <> 0;
	end;
	findClose(search);

	setLength(music, found.count - 2);

	i := 0;
	for m in found do
	begin
		n := found.extractName(m);
		v := found.values[n];
		if v <> 'ignore' then
		begin
			music[i].name := n;
			if v = 'usr' then music[i].user := true
			             else music[i].user := false;
			inc(i);
		end;
	end;

	found.free;

	for i := 0 to (length(music) - 1) do
	begin
		if music[i].user then path := getUserMusicInfoDir
		                 else path := MUSIC_INFO_DIR;

		loadMusicInfoFromFile(path + music[i].name, music[i]);
	end;

end;

{ ----------------------------------------------------------------------------
  Procedure to free memory used by resources.
  ---------------------------------------------------------------------------- }
procedure freeResources();
var
	m : ph_t_music_info;
begin

	info.free;
	music_tags.free;

	for m in music do
		m.info.free;

end;

{ ----------------------------------------------------------------------------
  Procedure to play a voice sound effect. The only reason to use this
  procedure instead of playSfx is that it plays with different volume,
  adequate to the voice sound effects.

  v  Index of the voice sound effect.
  ---------------------------------------------------------------------------- }
procedure playVoice(v : shortint);
begin

	snd_Play(getSound(v), FALSE, 0, 0, 0, VOICE_VOLUME);

end;

{ ----------------------------------------------------------------------------
  Procedure to play a sound effect.

  s  Index of the sound effect.
  ---------------------------------------------------------------------------- }
procedure playSfx(s : shortint);
begin

	snd_Play(getSound(s), FALSE, 0, 0, 0, SFX_VOLUME);

end;

{ ----------------------------------------------------------------------------
  Procedure to play a music track.

  m  Index of the music track.
  ---------------------------------------------------------------------------- }
procedure playMusic(m : smallint);
var
	path : string;
begin

	if m < 0 then exit;

	if snd_Get(SND_STREAM, music_id, SND_STATE_PLAYING) = 0 then
	begin
		path := getUserMusicAudioDir + music[m].info.values['file'];
		if fileExists(path) then
			music_id := snd_PlayFile(path, true, MUSIC_VOLUME)
		else
		begin
			path := MUSIC_AUDIO_DIR + music[m].info.values['file'];
			if fileExists(path) then
				music_id := snd_PlayFile(path, true, MUSIC_VOLUME);
		end;
	end;

end;

{ ----------------------------------------------------------------------------
  Procedure to stop the music track.
  ---------------------------------------------------------------------------- }
procedure stopMusic();
begin

	if snd_Get(SND_STREAM, music_id, SND_STATE_PLAYING) = 1 then
		snd_StopStream(music_id);

end;

{ ----------------------------------------------------------------------------
  Procedure to seek a position in the music track currently being played.

  ms  Position to seek (in milliseconds).
  ---------------------------------------------------------------------------- }
procedure seekMusic(ms : double);
begin

	if snd_Get(SND_STREAM, music_id, SND_STATE_PLAYING) = 1 then
		snd_SeekStream(music_id, ms);

end;

{ ----------------------------------------------------------------------------
  Procedure to initialize a list of the supported music info tags.
  ---------------------------------------------------------------------------- }
procedure initMusicTags();
begin

	music_tags := TStringList.create;
	music_tags.sorted := true;

	music_tags.add('album'  );
	music_tags.add('artist' );
	music_tags.add('file'   );
	music_tags.add('markers');
	music_tags.add('title'  );
	music_tags.add('year'   );

end;

{ ----------------------------------------------------------------------------
  Procedure to print a formatted error message when loading music info.

  error_string  String describing the error.
  file_name     String with the name of the file being read.
  line_number   Number of the line where the error ocurred.
  ---------------------------------------------------------------------------- }
procedure errorLoadingMusicInfo(const error_string : string;
                                const file_name    : string;
                                      line_number  : integer);
begin

	log_add('pascalhexagon: Error loading music info [' + file_name + ':' +
	        intToStr(line_number) + ']: ' + error_string);

end;

{ ----------------------------------------------------------------------------
  Procedure to load music info from a file.

  fname  String with the name of the file to be read.
  m      Where to store the music info.
  ---------------------------------------------------------------------------- }
procedure loadMusicInfoFromFile(const fname : string;
                                  var m     : ph_t_music_info);
var
	f        : text;
	str      : string;
	k, v     : string;
	i        : integer;
	line_num : integer = 0;
	char_pos : SizeInt;
	markers  : TStringList;
begin

	assign(f, fname);
	reset(f);

	m.info := TStringList.create;

	while not eof(f) do
	begin

		{ Read a line }
		readln(f, str);
		inc(line_num);

		{ Ignore empty lines }
		str := trim(str);
		if str = '' then continue;

		{ Check if there's a key=value pair }
		char_pos := pos('=', str);
		if char_pos = 0 then
		begin
			errorLoadingMusicInfo('Lines must be in key=value format.', fname, line_num);
			m.error := true;
			exit;
		end;

		k := trim(copy(str, 1, char_pos - 1));
		v := trim(copy(str, char_pos + 1, 255));

		{ Check if key is a supported music info tag }
		if not music_tags.find(k, i) then
		begin
			errorLoadingMusicInfo('Tag ' + k + ' is not supported.', fname, line_num);
			m.error := true;
			exit;
		end;

		m.info.values[k] := v;

	end;

	close(f);

	{ If 'file' tag is not present, create one with default extension }
	v := m.info.values['file'];
	if v = '' then
	begin
		v := m.name + DEFAULT_MUSIC_EXTENSION;
		m.info.values['file'] := v;
	end;

	if (not fileExists(getUserMusicAudioDir + v)) and
	   (not fileExists(MUSIC_AUDIO_DIR + v)) then
	begin
		errorLoadingMusicInfo('File ' + v + ' not found.', fname, line_num);
		m.error := true;
		exit;
	end;

	{ Check for markers }
	v := m.info.values['markers'];
	if v = '' then
	begin

		m.marker_count := 1;
		m.markers[0] := 0.0;
	end
	else
	begin
		markers := TStringList.create;
		markers.delimiter := ',';
		markers.strictDelimiter := true;
		markers.delimitedText := v;
		i := 0;
		for str in markers do
		begin
			m.markers[i] := strToFloat(str);
			inc(i);
		end;
		m.marker_count := i;
		markers.free;

		m.info.delete(m.info.indexOfName('markers'));
	end;

	log_add('pascalhexagon: Loaded music info for ' +
	        m.info.values['title'] + ' successfully.');

end;

end.
