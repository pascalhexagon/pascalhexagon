{ ----------------------------------------------------------------------------
  Unit that implements the in-game timer.
  ----------------------------------------------------------------------------
  Copyright 2016, 2017 Eduardo Mezêncio.

  This file is part of Pascal Hexagon.

  Pascal Hexagon is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Pascal Hexagon is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Pascal Hexagon.  If not, see <http://www.gnu.org/licenses/>.
  ----------------------------------------------------------------------------}

unit ph_timer;

//=========//
  INTERFACE
//=========//

uses
	ph_global;

procedure startTimer   ();
procedure updateTimer  (    dt   : double);
procedure readTimerStr (var dest : string);
procedure readLevelStr (var dest : string);

function  readTimer    () : double;
function  readLevel    () : shortint;

function  getNewRecord () : boolean;
procedure setNewRecord (val : boolean = true);


//==============//
  IMPLEMENTATION
//==============//

uses
	sysutils;

const
	//LEVEL_POINT    = 0;
	LEVEL_LINE     = 10000;
	LEVEL_TRIANGLE = 20000;
	LEVEL_SQUARE   = 30000;
	LEVEL_PENTAGON = 45000;
	LEVEL_HEXAGON  = 60000;

var
	time       : double;
	levels     : array[0..5] of string = ('POINT'   , 'LINE'    , 'TRIANGLE',
	                                      'SQUARE'  , 'PENTAGON', 'HEXAGON'  );
	new_record : boolean = false;

{ ----------------------------------------------------------------------------
  Procedure to reset the timer to zero.
  ---------------------------------------------------------------------------- }
procedure startTimer();
begin

	time       := 0;
	new_record := false;

end;

{ ----------------------------------------------------------------------------
  Procedure to update the timer. Should be called every frame.

  dt      Time elapsed since last frame (in milliseconds).
  ---------------------------------------------------------------------------- }
procedure updateTimer(dt : double);
begin

	time := time + dt;

end;

{ ----------------------------------------------------------------------------
  Procedure to get the current time as a string.

  dest    String to receive the current time.
  ---------------------------------------------------------------------------- }
procedure readTimerStr (var dest : string);
begin

	dest := floatToStrF(time / 1000, ffFixed, 0, 2);

end;

{ ----------------------------------------------------------------------------
  Procedure to get the string representing the current player level.

  dest    String to receive the current level.
  ---------------------------------------------------------------------------- }
procedure readLevelStr(var dest : string);
begin

	dest := levels[readLevel];

end;

{ ----------------------------------------------------------------------------
  Function to return the current time (in milliseconds).

  return  Returns the current time (in milliseconds).
  ---------------------------------------------------------------------------- }
function readTimer() : double;
begin

	readTimer := time;

end;

{ ----------------------------------------------------------------------------
  Function to return the current level as an integer number.

  return  Returns the current level number.
  ---------------------------------------------------------------------------- }
function readLevel() : shortint;
begin

	if      time < LEVEL_LINE     then
		readLevel := 0
	else if time < LEVEL_TRIANGLE then
		readLevel := 1
	else if time < LEVEL_SQUARE   then
		readLevel := 2
	else if time < LEVEL_PENTAGON then
		readLevel := 3
	else if time < LEVEL_HEXAGON  then
		readLevel := 4
	else
		readLevel := 5

end;

{ ----------------------------------------------------------------------------
  Function to return whether current time is a new record.

  return  Returns whether the current time is a new record.
  ---------------------------------------------------------------------------- }
function getNewRecord() : boolean;
begin

	getNewRecord := new_record;

end;

{ ----------------------------------------------------------------------------
  Procedure to set whether the current time is a new record.

  val     Whether the current time is a new record.
  ---------------------------------------------------------------------------- }
procedure setNewRecord(val : boolean);
begin

	new_record := val;

end;

end.
