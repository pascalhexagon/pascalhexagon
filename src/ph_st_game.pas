{ ----------------------------------------------------------------------------
  File: ph_st_game.pas

  Unit for the main game state. The interface contains only the procedure to
  activate this state. This procedure changes the update and draw callbacks to
  the ones implemented in this file. The transition from the game state to the
  game over state is also implmented here.
  ----------------------------------------------------------------------------
  Copyright 2016, 2017 Eduardo Mezêncio.

  This file is part of Pascal Hexagon.

  Pascal Hexagon is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Pascal Hexagon is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Pascal Hexagon.  If not, see <http://www.gnu.org/licenses/>.
  ---------------------------------------------------------------------------- }

unit ph_st_game;

//=========//
  INTERFACE
//=========//

procedure loadGame(retry : boolean = false);

//==============//
  IMPLEMENTATION
//==============//

uses
	{$IFDEF USE_ZENGL_STATIC}
	zgl_keyboard,
	zgl_main,
	zgl_render_2d,
	{$ELSE}
	zglHeader,
	{$ENDIF}
	ph_global,
	ph_helper,
	ph_resources,
	ph_colors,
	ph_camera,
	ph_polygon,
	ph_player,
	ph_obstacles,
	ph_course,
	ph_stage,
	ph_timer,
	ph_hud,
	ph_st_game_over;

const
	MAX_ZOOM_OUT_SPEED = 0.75 * Z_DISTANCE;

type
	t_transition_state = (pause, zoom_in, zoom_out, done);

var
	level                   : shortint;
	prev_level              : shortint;
	retrying                : boolean;
	new_record              : boolean;
	transition_pause_timer,
	transition_zoom_speed,
	transition_zoom_accel,
	transition_tilt_speed   : single;
	transition_state        : t_transition_state;
	slice_array             : ph_t_slice_array;
	depth_array             : ph_t_depth_array;


procedure init                     ();                forward;
procedure update                   (dt : double);     forward;
procedure draw                     ();                forward;
procedure initGameOverTransition   ();                forward;
procedure updateGameOverTransition (dt : double);     forward;
procedure drawGameOverTransition   ();                forward;
procedure loadGameOverTransition   ();                forward;
procedure gameOver                 ();                forward;

{ ----------------------------------------------------------------------------
  Procedure to set the state to the main game state, changing the callback
  functions update and draw accordingly.

  retry  Whether this is a retry (coming from game over screen).
  ---------------------------------------------------------------------------- }
procedure loadGame(retry : boolean);
begin

	zgl_reg(SYS_UPDATE, @update);
	zgl_reg(SYS_DRAW  , @draw  );

	retrying := retry;

	init;

end;

{ ----------------------------------------------------------------------------
  Procedure to initialize the main game state.
  ---------------------------------------------------------------------------- }
procedure init();
begin

	resetCamera;
	resetObstacles;
	resetPlayerAngle;
	resetStageLfosPhase;

	setPolygonAnimPause(true);

	setColorSwapTime(getStageColorSwapTime);

	level      := readLevel;
	prev_level := level;
	new_record := false;
	updateGameHudString;

	setCameraZoom(getGameOverZoom);
	transition_zoom_speed := 0;
	transition_zoom_accel := 0.05 * Z_DISTANCE * getScreenMin / REFERENCE_MIN;
	transition_state := zoom_out;

	initStageCourse;

	playVoice (VOICE_BEGIN);
	playSfx   (SFX_BEGIN);
	playMusic (getStageSoundtrack);

	if retrying then seekMusic(getStageRandomMarker)
	            else seekMusic(getStageFirstMarker);

	startTimer;

end;

{ ----------------------------------------------------------------------------
  Procedure to be used as update callback for the main game state. This is
  where the game logic is coded but, of course, most of it is done by calling
  functions from other units.

  dt  Time elapsed since last frame (in milliseconds).
  ---------------------------------------------------------------------------- }
procedure update(dt : double);
var
	dest,
	lfo_val_h,
	lfo_val_s,
	lfo_val_v,
	offset         : single;
	dist           : double;
	new_obstacles,
	i              : shortint;
begin

	{ Handle input }
	if      key_down  ( K_LEFT  ) then
	begin
		setPlayerDirection(left);
		dest := simplifyAngle(getPlayerAngle - dt * getStagePlayerSpeed / 1000);
		if collisionAt(dest) = false then
			movePlayerTo(dest);
	end
	else if key_down  ( K_RIGHT ) then
	begin
		setPlayerDirection(right);
		dest := simplifyAngle(getPlayerAngle + dt * getStagePlayerSpeed / 1000);
		if collisionAt(dest) = false then
			movePlayerTo(dest);
	end
	else
		setPlayerDirection(center);

	if      key_press ( K_ESCAPE ) then
	begin
		gameOver;
		exit;
	end;

	key_clearState;

	{ Stop course progression while polygon is transforming }
	if polygonChanging then
		dist := 0
	else
		dist := dt * getStageGameSpeed;

	{ Move the course's obstacles }
	moveObstacles(dist);

	{ Check for game over condition }
	if collisionAt(getPlayerAngle) then
	begin
		gameOver;
		exit;
	end;

	{ Create new obstacles if it's time to do so }
	new_obstacles := updateStageCourse(dist, slice_array, depth_array, offset);
	{ Less than zero means to change the number of sides of the polygon }
	if new_obstacles < 0 then
		createObstacle(new_obstacles, 0, offset)
	else
		for i := 0 to (new_obstacles - 1) do
			createObstacle(slice_array[i], depth_array[i], offset);

	{ Update the in-game timer }
	updateTimer(dt);
	prev_level := level;
	level      := readLevel;

	{ Check for new record }
	if not new_record then
	begin
		if readTimer > getStageBestTime then
		begin
			if getStageBestTime > 0 then
				playSfx(SFX_NEW_RECORD);
				playVoice(VOICE_EXCELLENT);
			new_record := true;
		end;
	end;

	{ Check for level change }
	if level <> prev_level then
	begin
		updateGameHudString;

		case level of
			1: playVoice(VOICE_LINE);
			2: playVoice(VOICE_TRIANGLE);
			3: playVoice(VOICE_SQUARE);
			4: playVoice(VOICE_PENTAGON);
			5: playVoice(VOICE_HEXAGON);
		end;
	end;

	{ Do camera zoom transitions }
	case transition_state of
		zoom_out:
		begin
			setCameraZoom(getCameraZoom + transition_zoom_speed * dt);
			if transition_zoom_speed > -MAX_ZOOM_OUT_SPEED then
				transition_zoom_speed := transition_zoom_speed - transition_zoom_accel;
			if getCameraZoom <= getGameZoom then
				transition_state := zoom_in;
		end;

		zoom_in:
		begin
			setCameraZoom(getCameraZoom + transition_zoom_speed * dt);
			transition_zoom_speed := transition_zoom_speed + transition_zoom_accel * 10;
			if getCameraZoom >= getGameZoom then
				transition_state := done;
		end;
	end;

	{ Update parameters that are controlled by LFOs }
	setPolygonExpansion(getStageLfoMix(STAGE_LFO_EXPANSION));

	setRotationSpeed(getStageLfoMix(STAGE_LFO_ROTATION));
	rotateCamera(dt);

	setTiltAngle(getStageLfoMix(STAGE_LFO_TILT_ANG));
	setTiltMagnitude(getStageLfoMix(STAGE_LFO_TILT_MAG));
	calculateTilt;

	updatePolygon(dt);
	updatePlayer;
	updateObstacles;

	lfo_val_h := getStageLfoMix(STAGE_LFO_HUE);
	lfo_val_s := getStageLfoMix(STAGE_LFO_SAT);
	lfo_val_v := getStageLfoMix(STAGE_LFO_VAL);

	setGameColors( getStageColor(STAGE_COLOR_H0) + lfo_val_h,
	               getStageColor(STAGE_COLOR_H1) + lfo_val_h,
	               limitRange(getStageColor(STAGE_COLOR_S0) + lfo_val_s, 0, 1),
	               limitRange(getStageColor(STAGE_COLOR_S1) + lfo_val_s, 0, 1),
	               limitRange(getStageColor(STAGE_COLOR_V0) + lfo_val_v, 0, 1),
	               limitRange(getStageColor(STAGE_COLOR_V1) + lfo_val_v, 0, 1)  );

	updateColorSwap(dt);

end;

{ ----------------------------------------------------------------------------
  Procedure to be used as draw callback for the main game state.
  ---------------------------------------------------------------------------- }
procedure draw();
begin

	batch2d_begin;

	useCamera;

	drawPolygon;
	drawPlayer;
	drawObstacles;

	useNoCamera;

	drawGameHud;

	batch2d_end;

end;

{ ----------------------------------------------------------------------------
  Procedure to initialize the game over transition sub-state.
  ---------------------------------------------------------------------------- }
procedure initGameOverTransition();
begin

	transition_pause_timer := 500;
	transition_zoom_speed  := 0;
	transition_zoom_accel  := 0.01 * Z_DISTANCE;

	if getTiltMagnitude < 0 then
	begin
		setTiltMagnitude(-getTiltMagnitude);
		setTiltAngle(getTiltAngle + 180);
	end;
	transition_tilt_speed  := getTiltMagnitude / 1000;

	transition_state       := pause;

	setRotationSpeed(getStageLfoMix(STAGE_LFO_ROTATION));

end;

{ ----------------------------------------------------------------------------
  Procedure to update the game over transition sub-state.

  dt  Time elapsed since last frame (in milliseconds).
  ---------------------------------------------------------------------------- }
procedure updateGameOverTransition(dt : double);
begin

	if key_press ( K_ESCAPE ) or
	   key_press ( K_ENTER  ) then
	begin
		if transition_state = pause then
			setRotationSpeed( -(getRotationSpeed / 4) );

		resetObstacles;
		loadGameOver;
		key_clearState;
		exit;
	end;

	key_clearState;

	rotateCamera(dt);

	if transition_state = pause then
	begin
		if transition_pause_timer > 0 then
			transition_pause_timer := transition_pause_timer - dt
		else
		begin
			transition_state := zoom_in;
			setRotationSpeed( -(getRotationSpeed / 4) );
		end;
	end
	else
	begin
		transition_zoom_speed := transition_zoom_speed + transition_zoom_accel;
		setCameraZoom(getCameraZoom + dt * transition_zoom_speed);
		moveObstacles( -(dt * transition_zoom_speed) / 256 );
	end;

	if getTiltMagnitude > 0 then
		setTiltMagnitude(getTiltMagnitude - transition_tilt_speed * dt);
	calculateTilt;
	updatePolygon(dt);
	updatePlayer;
	updateObstacles;

	if getCameraZoom >= getGameOverZoom then
	begin
		resetObstacles;
		loadGameOver;
	end;

end;

{ ----------------------------------------------------------------------------
  Procedure to draw the game over transition sub-state.
  ---------------------------------------------------------------------------- }
procedure drawGameOverTransition();
begin

	draw;

end;

{ ----------------------------------------------------------------------------
  Procedure to set the state to the game over transition sub-state, changing
  the callback functions update and draw accordingly. I call it a sub-state
  since it is just a variation of the main game state.
  ---------------------------------------------------------------------------- }
procedure loadGameOverTransition();
begin

	zgl_reg(SYS_UPDATE, @updateGameOverTransition);
	zgl_reg(SYS_DRAW  , @drawGameOverTransition  );

	initGameOverTransition;

end;

{ ----------------------------------------------------------------------------
  Procedure to call when the game is over. It will change the state to the
  game over transition state, which will call the game over state after the
  transition is over.
  ---------------------------------------------------------------------------- }
procedure gameOver();
begin

	playSfx(SFX_COLLISION);
	stopMusic;
	setPolygonExpansion(0);

	loadGameOverTransition;
	key_clearState;

	if new_record then
	begin
		setStageBestTime(readTimer);
		setNewRecord;
	end;

end;

end.
