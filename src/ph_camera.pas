{ ----------------------------------------------------------------------------
  File: ph_camera.pas

  Unit that implements a camera for the game.
  ----------------------------------------------------------------------------
  Copyright 2016, 2017 Eduardo Mezêncio.

  This file is part of Pascal Hexagon.

  Pascal Hexagon is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Pascal Hexagon is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Pascal Hexagon.  If not, see <http://www.gnu.org/licenses/>.
  ---------------------------------------------------------------------------- }

unit ph_camera;

//=========//
  INTERFACE
//=========//

uses
	ph_global;

const
	Z_DISTANCE       = 1;

	REFERENCE_WIDTH  = 800;
	REFERENCE_HEIGHT = 600;
	REFERENCE_MAX    = REFERENCE_WIDTH;
	REFERENCE_MIN    = REFERENCE_HEIGHT;
	REFERENCE_MEAN   = (REFERENCE_WIDTH + REFERENCE_HEIGHT) div 2;
	REFERENCE_ASPECT = (REFERENCE_WIDTH / REFERENCE_HEIGHT);


function  getCameraZoom      () : single;
function  getGameZoom        () : single;
function  getGameOverZoom    () : single;
function  getCameraAngle     () : single;
function  getRotationSpeed   () : single;
function  getTiltAngle       () : single;
function  getTiltMagnitude   () : single;
function  getScreenWidth     () : integer;
function  getScreenHeight    () : integer;
function  getScreenMin       () : integer;
function  getScreenMax       () : integer;
function  getScreenMeanRatio () : single;

procedure setCameraZoom      (zoom  : single);
procedure setCameraAngle     (angle : single);
procedure setRotationSpeed   (speed : single);
procedure setTiltAngle       (angle : single);
procedure setTiltMagnitude   (mag   : single);
procedure setScreenSize      (w, h  : integer);

procedure rotateCamera       (dt    : double);
procedure calculateTilt      ();
procedure projectPoint       (var p : TPoint);

procedure useCamera          ();
procedure useNoCamera        ();
procedure resetCamera        ();

//==============//
  IMPLEMENTATION
//==============//

uses
	math,
	{$IFDEF USE_ZENGL_STATIC}
	zgl_camera_2d,
	{$ELSE}
	zglHeader,
	{$ENDIF}
	ph_helper;

const
	TILT_CAMERA_ADJUSTMENT    = 4;
	GAME_ZOOM_MULTIPLIER      = 64;
	GAME_OVER_ZOOM_MULTIPLIER = 288;

var
	camera            : TCamera;
	game_zoom         : single;
	game_over_zoom    : single;
	rotation_speed    : single;
	tilt_angle        : single;
	tilt_magnitude    : single;
	tilt_x            : single;
	tilt_y            : single;
	pos_x             : single;
	pos_y             : single;
	screen_width      : integer;
	screen_height     : integer;
	screen_min        : integer;
	screen_max        : integer;
	screen_mean       : integer;
	screen_mean_ratio : single;


{ ----------------------------------------------------------------------------
  Function to return the current zoom of the camera.
  ---------------------------------------------------------------------------- }
function getCameraZoom() : single;
begin

	getCameraZoom := camera.zoom.x;

end;

{ ----------------------------------------------------------------------------
  Function to return the zoom value to use during the game.
  ---------------------------------------------------------------------------- }
function getGameZoom() : single;
begin

	getGameZoom := game_zoom;

end;

{ ----------------------------------------------------------------------------
  Function to return the zoom value to use in the game over screen.
  ---------------------------------------------------------------------------- }
function getGameOverZoom() : single;
begin

	getGameOverZoom := game_over_zoom;

end;

{ ----------------------------------------------------------------------------
  Function to return the camera's current rotation angle (in degrees).
  ---------------------------------------------------------------------------- }
function getCameraAngle() : single;
begin

	getCameraAngle := camera.angle;

end;

{ ----------------------------------------------------------------------------
  Function to return the camera's current rotation speed (in degrees/second).
  ---------------------------------------------------------------------------- }
function getRotationSpeed() : single;
begin

	getRotationSpeed := rotation_speed;

end;

{ ----------------------------------------------------------------------------
  Function to return the camera's current tilt angle (in degrees).
  ---------------------------------------------------------------------------- }
function getTiltAngle() : single;
begin

	getTiltAngle := tilt_angle;

end;

{ ----------------------------------------------------------------------------
  Function to return the camera's current tilt magnitude.
  ---------------------------------------------------------------------------- }
function getTiltMagnitude() : single;
begin

	getTiltMagnitude := tilt_magnitude;

end;

{ ----------------------------------------------------------------------------
  Function to return the screen width.
  ---------------------------------------------------------------------------- }
function getScreenWidth() : integer;
begin

	getScreenWidth := screen_width;

end;

{ ----------------------------------------------------------------------------
  Function to return the screen height.
  ---------------------------------------------------------------------------- }
function getScreenHeight() : integer;
begin

	getScreenHeight := screen_height;

end;

{ ----------------------------------------------------------------------------
  Function to return the minimal value between screen width and height.
  ---------------------------------------------------------------------------- }
function getScreenMin() : integer;
begin

	getScreenMin := screen_min;

end;

{ ----------------------------------------------------------------------------
  Function to return the maximal value between screen width and height.
  ---------------------------------------------------------------------------- }
function getScreenMax() : integer;
begin

	getScreenMax := screen_max;

end;

{ ----------------------------------------------------------------------------
  Function to get the ratio between the mean of the screen dimensions and the
  mean of the dimensions used as reference for the game, used to calculate the
  size of some things on the screen.
  ---------------------------------------------------------------------------- }
function getScreenMeanRatio() : single;
begin

	getScreenMeanRatio := screen_mean_ratio;

end;

{ ----------------------------------------------------------------------------
  Procedure to set the camera's current zoom.
  ---------------------------------------------------------------------------- }
procedure setCameraZoom(zoom : single);
begin

	camera.zoom.x := zoom;
	camera.zoom.y := zoom;

end;

{ ----------------------------------------------------------------------------
  Procedure to set the camera's current rotation angle (in degrees).
  ---------------------------------------------------------------------------- }
procedure setCameraAngle(angle : single);
begin

	camera.angle := angle;

end;

{ ----------------------------------------------------------------------------
  Procedure to set the camera's current rotation speed (in degrees/second).
  ---------------------------------------------------------------------------- }
procedure setRotationSpeed(speed : single);
begin

	rotation_speed := speed;

end;

{ ----------------------------------------------------------------------------
  Procedure to set the camera's current tilt angle (in degrees).
  ---------------------------------------------------------------------------- }
procedure setTiltAngle(angle : single);
begin

	tilt_angle := angle;

end;

{ ----------------------------------------------------------------------------
  Procedure to set the camera's current tilt magnitude.
  ---------------------------------------------------------------------------- }
procedure setTiltMagnitude(mag : single);
begin

	tilt_magnitude := mag;

end;

{ ----------------------------------------------------------------------------
  Procedure to set the screen size.

  w       Screen width.
  h       Screen height.
  ---------------------------------------------------------------------------- }
procedure setScreenSize(w, h : integer);
begin

	pos_x             := -(w / 2);
	pos_y             := -(h / 2);

	screen_width      := w;
	screen_height     := h;
	screen_min        := min(w, h);
	screen_max        := max(w, h);
	screen_mean       := (w + h) div 2;
	screen_mean_ratio := screen_mean / REFERENCE_MEAN;

	game_zoom         := GAME_ZOOM_MULTIPLIER      * Z_DISTANCE * screen_mean / REFERENCE_MEAN;
	game_over_zoom    := GAME_OVER_ZOOM_MULTIPLIER * Z_DISTANCE * screen_min  / REFERENCE_MIN;

end;

{ ----------------------------------------------------------------------------
  Procedure to change the angle of the camera in proportion to the current
  camera rotation speed and to the change in time.

  dt  Change in time (in milliseconds).
  ---------------------------------------------------------------------------- }
procedure rotateCamera(dt : double);
begin

	with camera do
	begin
		angle := angle + rotation_speed * (dt / 1000);
		if      angle >= 360 then angle := angle - 360
		else if angle <  0   then angle := angle + 360;
	end;

end;

{ ----------------------------------------------------------------------------
  Procedure to update the tilt values. They are used to simulate perspective.
  For now, while the projection is done by the function projectPoint, the
  rotation of the camera is done by ZenGL, so to rotate the game elements
  without rotating the camera tilt angle with it, these adjustments to camera
  and tilt values are needed. (Also the perspective projection sucks. See
  the projectPoint procedure)
  ---------------------------------------------------------------------------- }
procedure calculateTilt();
var
	angle : single;
begin

	angle    := (tilt_angle - camera.angle) * PI / 180;
	tilt_x   := cos(angle) * tilt_magnitude * Z_DISTANCE;
	tilt_y   := sin(angle) * tilt_magnitude * Z_DISTANCE;
	camera.x := pos_x - tilt_x * TILT_CAMERA_ADJUSTMENT;
	camera.y := pos_y - tilt_y * TILT_CAMERA_ADJUSTMENT;

end;

{ ----------------------------------------------------------------------------
  Procedure to project a point to simulate perspective. For now, the
  perspective projection sucks, because it just assigns a z coordinate to the
  point based on the tilt values and then do the z division. Since this is done
  without a proper rotation (without changing the x and y values accordingly),
  the screen looks more distorted as the tilt angle inscreases, meaning that
  the game only looks good with small tilt angles.

  p  The point to project.
  ---------------------------------------------------------------------------- }
procedure projectPoint(var p : TPoint);
var
	z : single;
begin

	z := tilt_x * p.x + tilt_y * p.y + Z_DISTANCE;

	if z <= 0 then
		z := 1 / 1024;

	p.x := p.x / z;
	p.y := p.y / z;

end;

{ ----------------------------------------------------------------------------
  Procedure to turn on the camera transform.
  ---------------------------------------------------------------------------- }
procedure useCamera();
begin

	cam2d_set(@camera);

end;

{ ----------------------------------------------------------------------------
  Procedure to turn off the camera transform.
  ---------------------------------------------------------------------------- }
procedure useNoCamera();
begin

	cam2d_set(nil);

end;

{ ----------------------------------------------------------------------------
  Procedure to set the camera to default values.
  ---------------------------------------------------------------------------- }
procedure resetCamera();
begin

	rotation_speed := 0;

	with camera do
	begin
		x        := pos_x;
		y        := pos_y;
		center.x := screen_width  / 2;
		center.y := screen_height / 2;
		angle    := 0;
		zoom.x   := game_zoom;
		zoom.y   := game_zoom;
	end;

end;

end.
