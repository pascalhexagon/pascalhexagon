{ ----------------------------------------------------------------------------
  File: ph_st_game_over.pas

  Unit for the game over state. The interface contains only the procedure to
  activate this state. This procedure changes the update and draw callbacks to
  the ones implemented in this file.
  ----------------------------------------------------------------------------
  Copyright 2016, 2017 Eduardo Mezêncio.

  This file is part of Pascal Hexagon.

  Pascal Hexagon is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Pascal Hexagon is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Pascal Hexagon.  If not, see <http://www.gnu.org/licenses/>.
  ---------------------------------------------------------------------------- }

unit ph_st_game_over;

//=========//
  INTERFACE
//=========//

procedure loadGameOver();

//==============//
  IMPLEMENTATION
//==============//

uses
	{$IFDEF USE_ZENGL_STATIC}
	zgl_keyboard,
	zgl_main,
	zgl_render_2d,
	{$ELSE}
	zglHeader,
	{$ENDIF}
	ph_global,
	ph_resources,
	ph_camera,
	ph_polygon,
	ph_player,
	ph_hud,
	ph_st_stage_select,
	ph_st_game;


procedure init   ();            forward;
procedure update (dt : double); forward;
procedure draw   ();            forward;

{ ----------------------------------------------------------------------------
  Procedure to set the state to the game over state, changing the callback
  functions update and draw accordingly.
  ---------------------------------------------------------------------------- }
procedure loadGameOver();
begin

	zgl_reg(SYS_UPDATE, @update);
	zgl_reg(SYS_DRAW  , @draw  );

	init;

end;

{ ----------------------------------------------------------------------------
  Procedure to initialize the game over state.
  ---------------------------------------------------------------------------- }
procedure init();
begin

	setCameraZoom(getGameOverZoom);
	setRotationSpeed( -(getRotationSpeed * 2) );

	setTiltMagnitude(0);
	calculateTilt;

	updatePolygon(0);
	updatePlayer;

	playVoice(VOICE_GAME_OVER);

end;

{ ----------------------------------------------------------------------------
  Procedure to be used as update callback for the game over state.

  dt  Time elapsed since last frame (in milliseconds).
  ---------------------------------------------------------------------------- }
procedure update(dt : double);
begin

	if      key_press ( K_ENTER  ) then
	begin
		loadGame(true);
		key_clearState;
		exit;
	end
	else if key_press ( K_ESCAPE ) then
	begin
		loadStageSelect;
		key_clearState;
		exit;
	end;

	key_clearState;

	rotateCamera(dt);
	updateHudColor;

end;

{ ----------------------------------------------------------------------------
  Procedure to be used as draw callback for the game over state.
  ---------------------------------------------------------------------------- }
procedure draw();
begin

	batch2d_begin;

	useCamera;

	drawPolygon;
	drawPlayer;

	useNoCamera;

	drawGameOverHud;

	batch2d_end;

end;

end.
