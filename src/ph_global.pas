{ ----------------------------------------------------------------------------
  File: ph_global.pas

  Unit with constants and generic types.
  ----------------------------------------------------------------------------
  Copyright 2016, 2017 Eduardo Mezêncio.

  This file is part of Pascal Hexagon.

  Pascal Hexagon is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Pascal Hexagon is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Pascal Hexagon.  If not, see <http://www.gnu.org/licenses/>.
  ---------------------------------------------------------------------------- }

unit ph_global;

//=========//
  INTERFACE
//=========//

uses
	sysutils,
	{$IFDEF USE_ZENGL_STATIC}
	zgl_camera_2d,
	zgl_font,
	zgl_math_2d,
	zgl_sound;
	{$ELSE}
	zglHeader;
	{$ENDIF}

const
	WINDOW_TITLE = 'Pascal Hexagon';
	GAME_AUTHOR  = 'EDUARDO MEZENCIO';
	GAME_YEAR    = '2016, 2017';

	{$IFDEF WINDOWS}
	SLASH = '\';
	{$ELSE}
	SLASH = '/';
	{$ENDIF}

	{ TODO: change system folders to work prolerly when game is installed.
	  Somehow, these folders will have to be defined by the installation. }
	DATA_DIR        = '.' + SLASH + 'data' + SLASH;
	STAGES_DIR      = DATA_DIR  + 'stages' + SLASH;
	MUSIC_DIR       = DATA_DIR  + 'music'  + SLASH;
	MUSIC_AUDIO_DIR = MUSIC_DIR + 'audio'  + SLASH;
	MUSIC_INFO_DIR  = MUSIC_DIR + 'info'   + SLASH;

type
	TPoint         = ZglTPoint2D;
	PPoint         = ZglPPoint2D;
	TCamera        = ZglTCamera2D;
	PCamera        = ZglPCamera2D;
	PFont          = ZglPFont;
	PSound         = ZglPSound;

	PShortInt      = ^ShortInt;
	PSmallInt      = ^SmallInt;
	PSingle        = ^Single;
	PDouble        = ^Double;
	PShortString   = ^ShortString;

//==============//
  IMPLEMENTATION
//==============//

end.

