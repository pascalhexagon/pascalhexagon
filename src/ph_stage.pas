{ ----------------------------------------------------------------------------
  File: ph_stage.pas

  Unit that implements stages and operations to handle them. This unit is used
  to load stages from disk, accessing information from stages and saving and
  loading best times.
  ----------------------------------------------------------------------------
  Copyright 2016, 2017 Eduardo Mezêncio.

  This file is part of Pascal Hexagon.

  Pascal Hexagon is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Pascal Hexagon is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Pascal Hexagon.  If not, see <http://www.gnu.org/licenses/>.
  ---------------------------------------------------------------------------- }

unit ph_stage;

//=========//
  INTERFACE
//=========//

uses
	ph_global,
	ph_lfo,
	ph_course;

const
	STAGE_COLOR_H0              = 0;
	STAGE_COLOR_H1              = 1;
	STAGE_COLOR_S0              = 2;
	STAGE_COLOR_S1              = 3;
	STAGE_COLOR_V0              = 4;
	STAGE_COLOR_V1              = 5;

	STAGE_LFO_EXPANSION         = 0;
	STAGE_LFO_HUE               = 1;
	STAGE_LFO_SAT               = 2;
	STAGE_LFO_VAL               = 3;
	STAGE_LFO_ROTATION          = 4;
	STAGE_LFO_TILT_ANG          = 5;
	STAGE_LFO_TILT_MAG          = 6;
	STAGE_TOTAL_LFO             = 7; {< Number things that use LFOs }
	STAGE_MAX_LFO               = 8; {< Maximum of LFO for each thing }
	STAGE_TOTAL_LFO_INDEXES     = STAGE_MAX_LFO * STAGE_TOTAL_LFO;

	STAGE_MUSIC_INFO_TITLE      = 0;
	STAGE_MUSIC_INFO_ARTIST     = 1;
	STAGE_MUSIC_INFO_ALBUM_YEAR = 2;


type
	{ Types just to be used in ph_t_stage }
	ph_t_lfo_array    = array[0..(STAGE_TOTAL_LFO_INDEXES - 1)] of ph_t_lfo;
	ph_t_lfo_counts   = array[0..(STAGE_TOTAL_LFO - 1)] of shortint;
	ph_t_lfo_defaults = array[0..(STAGE_TOTAL_LFO - 1)] of single;
	ph_t_color_array  = array[0..5] of single;

	ph_p_stage = ^ph_t_stage;
	{ ------------------------------------------------------------------------
	  Record that represents a stage.
	  ------------------------------------------------------------------------ }
	ph_t_stage = record
		name            : string;   {< Name of the stage (and file). }
		user            : boolean;  {< Whether stage was loaded from user folder. }
		error           : boolean;  {< Whether there was an error loading the stage. }
		hash            : string;   {< Hash of the stage, used to store best times. }
		game_speed      : single;   {< Speed of the game in this stage. }
		player_speed    : single;   {< Speed of the player movement in this stage. }
		lfos            : ph_t_lfo_array; {< LFOs to control animation. }
		lfo_count       : ph_t_lfo_counts; {< Number of LFOs for each purpose. }
		lfo_default_val : ph_t_lfo_defaults; {< Default values of LFOs for each purpose. }
		colors          : ph_t_color_array; {< Colors for this stage (indexes are STAGE_COLOR_ constants). }
		color_swap      : double;   {< Color swap time for this stage. }
		best_time       : single;   {< Best time for this stage. }
		course          : ph_t_course; {< The course, main part of the stage. }
		soundtrack      : smallint; {< Index of the stage's music. }
	end;


procedure loadStagesFromDisk    ();
procedure setCurrentStage       (n  : smallint);

function  getCurrentStageIndex  ()               : smallint;
function  getStagesCount        ()               : smallint;
function  getStageName          ()               : string;
function  getStageSoundtrack    ()               : smallint;
function  getStageFirstMarker   ()               : double;
function  getStageRandomMarker  ()               : double;
function  getStageError         ()               : boolean;
function  getStageColor         (id : smallint)  : single;
function  getStageColorSwapTime ()               : double;
function  getStagePlayerSpeed   ()               : single;
function  getStageGameSpeed     ()               : single;
function  getStageLfoMix        (id : smallint)  : single;
function  getStageInitSides     ()               : shortint;
function  getStageMusicString   (id : smallint)  : PShortString;

function  getStageBestTime      ()               : single;
procedure setStageBestTime      (time : single);

procedure resetStageLfosPhase   ();

procedure initStageCourse       ();
function  updateStageCourse     (    dist   : double;
                                 var slice  : ph_t_slice_array;
                                 var depth  : ph_t_depth_array;
                                 var offset : single           ) : shortint;

//==============//
  IMPLEMENTATION
//==============//

uses
	sysutils,
	classes,
	md5,
	{$IFDEF USE_ZENGL_STATIC}
	zgl_log,
	{$ELSE}
	zglHeader,
	{$ENDIF}
	ph_resources,
	ph_user_data,
	ph_file,
	ph_polygon,
	ph_stage_parser;

type
	t_bytes_4 = packed array[0..3] of char;

var
	stages         : array of ph_t_stage;
	current        : ph_p_stage;
	current_index  : shortint;
	music_info_str : array [0..2] of string;


procedure saveBestTimeToFile   (); forward;
procedure loadBestTimeFromFile (var s : ph_t_stage); forward;


{ ----------------------------------------------------------------------------
  Procedure to load stages from disk and their best times. It searches the
  system folder and the user folder for stages. If a stage is found with the
  same name in both folders, it will load the one from the user folder.
  ---------------------------------------------------------------------------- }
procedure loadStagesFromDisk();
var
	error_stage  : ph_t_stage;
	search       : tSearchRec;
	stages_found : tStringList;
	path         : string;
	i            : integer;
	s, n, v      : string;
begin

	{ Create a template to copy to stages with errors }
	with error_stage do
	begin
		error := true;

		for i := 0 to (STAGE_TOTAL_LFO - 1) do
		begin
			lfo_count[i]       := 0;
			lfo_default_val[i] := 0;
		end;

		course.init_sides := MAX_POLYGON;

		colors[STAGE_COLOR_H0] := 0;
		colors[STAGE_COLOR_H1] := 0;
		colors[STAGE_COLOR_S0] := 0.50;
		colors[STAGE_COLOR_S1] := 0.20;
		colors[STAGE_COLOR_V0] := 0.50;
		colors[STAGE_COLOR_V1] := 0.80;

	end;

	stages_found := tStringList.create;
	stages_found.sorted := true;
	stages_found.add( '.=ignore');
	stages_found.add('..=ignore');

	{ Search system stages }
	if findFirst(STAGES_DIR + '*', faAnyFile, search) = 0 then
	begin
		repeat
			if stages_found.values[search.name] <> 'ignore' then
			   stages_found.values[search.name] := 'sys';
		until findNext(search) <> 0;
	end;
	findClose(search);

	{ Search user stages }
	if findFirst(getUserStagesDir + '*', faAnyFile, search) = 0 then
	begin
		repeat
			if stages_found.values[search.name] <> 'ignore' then
			   stages_found.values[search.name] := 'usr';
		until findNext(search) <> 0;
	end;
	findClose(search);

	setLength(stages, stages_found.count - 2);

	{ Initialize each stage with its name }
	i := 0;
	for s in stages_found do
	begin
		n := stages_found.extractName(s);
		v := stages_found.values[n];
		if v <> 'ignore' then
		begin
			stages[i].name := n;
			if v = 'usr' then stages[i].user := true
			             else stages[i].user := false;
			stages[i].error := false;
			inc(i);
		end;
	end;

	stages_found.free;

	{ If no stages were found }
	if length(stages) = 0 then
	begin
		setLength(stages, 1);
		stages[0] := error_stage;
		stages[0].name  := 'NO STAGES';
		exit;
	end;

	{ Load each stage }
	for i := 0 to (length(stages) - 1) do
	begin
		if stages[i].user then path := getUserStagesDir
		                  else path := STAGES_DIR;

		loadStageFromFile(path + stages[i].name, stages[i]);

		if stages[i].error then
		begin
			error_stage.name := stages[i].name;
			stages[i] := error_stage;
		end
		else
			loadBestTimeFromFile(stages[i]);
	end;

end;

{ ----------------------------------------------------------------------------
  Procedure to change the current stage.

  n  Index of the new stage.
  ---------------------------------------------------------------------------- }
procedure setCurrentStage(n : smallint);
var
	m    : smallint;
	a, y : string;
begin

	current := @stages[n];
	current_index := n;

	m := getStageSoundtrack;
	if m >= 0 then
	begin
		getMusicInfo(m, 'title', music_info_str[STAGE_MUSIC_INFO_TITLE]);
		getMusicInfo(m, 'artist', music_info_str[STAGE_MUSIC_INFO_ARTIST]);
		getMusicInfo(m, 'album', a);
		getMusicInfo(m, 'year', y);
	end
	else
	begin
		music_info_str[STAGE_MUSIC_INFO_TITLE]  := 'MUSIC ERROR';
		music_info_str[STAGE_MUSIC_INFO_ARTIST] := '';
		a := '';
		y := '';
	end;

	if (a = '') and (y = '') then
		music_info_str[STAGE_MUSIC_INFO_ALBUM_YEAR] := ''
	else if a = '' then
		music_info_str[STAGE_MUSIC_INFO_ALBUM_YEAR] := y
	else if y = '' then
		music_info_str[STAGE_MUSIC_INFO_ALBUM_YEAR] := a
	else
		music_info_str[STAGE_MUSIC_INFO_ALBUM_YEAR] := a + ' - ' + y;

end;

{ ----------------------------------------------------------------------------
  Function to return the current stage's index.

  return  Return the index of the current stage.
  ---------------------------------------------------------------------------- }
function getCurrentStageIndex() : smallint;
begin

	getCurrentStageIndex := current_index;

end;

{ ----------------------------------------------------------------------------
  Function to return the total number of stages.

  return  Returns the number of stages.
  ---------------------------------------------------------------------------- }
function getStagesCount() : smallint;
begin

	getStagesCount := length(stages)

end;

{ ----------------------------------------------------------------------------
  Function to return the current stage's name.

  return  Returns the name of the current stage.
  ---------------------------------------------------------------------------- }
function getStageName() : string;
begin

	getStageName := current^.name;

end;

{ ----------------------------------------------------------------------------
  Function to return the index of the current stage's music.

  return  Returns the index of the music of the current stage.
  ---------------------------------------------------------------------------- }
function getStageSoundtrack() : smallint;
begin

	getStageSoundtrack := current^.soundtrack;

end;

{ ----------------------------------------------------------------------------
  Function to return the first marker of the current stage's music.

  return  Returns the first marker of the current music.
  ---------------------------------------------------------------------------- }
function getStageFirstMarker() : double;
begin

	getStageFirstMarker := getMusicFirstMarker(current^.soundtrack);

end;

{ ----------------------------------------------------------------------------
  Function to return a random marker of the current stage's music.

  return  Returns a random marker of the current music.
  ---------------------------------------------------------------------------- }
function getStageRandomMarker() : double;
begin

	getStageRandomMarker := getMusicRandomMarker(current^.soundtrack);

end;

{ ----------------------------------------------------------------------------
  Function to check whether there was an error loading the current stage.

  return  Returns whether the current stage has an error.
  ---------------------------------------------------------------------------- }
function getStageError() : boolean;
begin

	getStageError := current^.error;

end;

{ ----------------------------------------------------------------------------
  Function to get a color from the current stage.

  id      Id of the color to get.

  return  Returns the current stage's color of given id.
  ---------------------------------------------------------------------------- }
function getStageColor(id : smallint) : single;
begin

	getStageColor := current^.colors[id];

end;

{ ----------------------------------------------------------------------------
  Function to return the current stage's color swap time.

  return  Returns the color swap time of current stage.
  ---------------------------------------------------------------------------- }
function getStageColorSwapTime() : double;
begin

	getStageColorSwapTime := current^.color_swap;

end;

{ ----------------------------------------------------------------------------
  Function to return the current stage's player speed.

  return  Returns the player speed of current stage.
  ---------------------------------------------------------------------------- }
function getStagePlayerSpeed() : single;
begin

	getStagePlayerSpeed := current^.player_speed;

end;

{ ----------------------------------------------------------------------------
  Function to return the current stage's game speed.

  return  Returns the game speed of current stage.
  ---------------------------------------------------------------------------- }
function getStageGameSpeed() : single;
begin

	getStageGameSpeed := current^.game_speed;

end;

{ ----------------------------------------------------------------------------
  Function to get the mix (sum) of a given set of LFO's from current stage.

  id      Id of the LFO set (starts with STAGE_LFO_)

  return  Returns the mix (sum) of the LFOs.
  ---------------------------------------------------------------------------- }
function getStageLfoMix(id : smallint) : single;
var
	val : single;
begin

	with current^ do
	begin
		if lfo_count[id] = 0 then
			val := lfo_default_val[id]
		else
			val := lfoMix(@(lfos[id * STAGE_MAX_LFO]), lfo_count[id]);
	end;

	getStageLfoMix := val;

end;

{ ----------------------------------------------------------------------------
  Function to return the initial number of sides of the polygon for the
  current stage.

  return  Returns the initial number of sides of the polygon.
  ---------------------------------------------------------------------------- }
function getStageInitSides() : shortint;
begin

	getStageInitSides := current^.course.init_sides;

end;

{ ----------------------------------------------------------------------------
  Function to get a string of music info for current stage's music.

  id      Id of the music info (starts with STAGE_MUSIC_INFO_)

  return  Returns a pointer to the string.
  ---------------------------------------------------------------------------- }
function getStageMusicString(id : smallint) : PShortString;
begin

	getStageMusicString := @(music_info_str[id]);

end;

{ ----------------------------------------------------------------------------
  Function to return the current stage's best time (in milliseconds).

  return  Returns the current stage's best time (in milliseconds).
  ---------------------------------------------------------------------------- }
function getStageBestTime() : single;
begin

	getStageBestTime := current^.best_time;

end;

{ ----------------------------------------------------------------------------
  Procedure to change the current stage's best time (in milliseconds).

  time  New best time (in milliseconds).
  ---------------------------------------------------------------------------- }
procedure setStageBestTime(time : single);
begin

	current^.best_time := time;
	saveBestTimeToFile;

end;

{ ----------------------------------------------------------------------------
  Procedure to reset the phase of all LFOs of current stage.
  ---------------------------------------------------------------------------- }
procedure resetStageLfosPhase();
var
	i : integer;
begin

	for i := 0 to (STAGE_TOTAL_LFO_INDEXES - 1) do
		resetLfoPhase(current^.lfos[i]);

end;

{ ----------------------------------------------------------------------------
  Procedure to initialize the current stage.
  ---------------------------------------------------------------------------- }
procedure initStageCourse();
begin

	initCourse(current^.course);

end;

{ ----------------------------------------------------------------------------
  Function to call updateCourse on current stage and return the result.

  dist    How much to advance the course.
  slice   Array of slices where obstacles will be created.
  depth   Array of depth of obstacles that will be created.
  offset  Adjustment in distance of slices that will be created.

  return  Returns the number of obstacles to be created.
  ---------------------------------------------------------------------------- }
function updateStageCourse(    dist   : double;
                           var slice  : ph_t_slice_array;
                           var depth  : ph_t_depth_array;
                           var offset : single           ) : shortint;
begin

	updateStageCourse := updateCourse(current^.course,
	                                  dist , slice ,
	                                  depth, offset);

end;

{ ----------------------------------------------------------------------------
  Procedure to save the best time of current stage to a file. This procedure
  creates a simple binary file with the time and a hash of the stage. The hash
  is used to avoid loading this same time if the stage is altered.
  ---------------------------------------------------------------------------- }
procedure saveBestTimeToFile();
var
	f     : file of char;
	fname : shortString;
	data  : shortString;
	hash  : shortString;
	time  : shortString;
	i     : smallint;
begin

	for i := 0 to 3 do
		time[i + 1] := t_bytes_4(current^.best_time)[i];
	time[0] := char(4);

	data := current^.hash + time;
	hash := md5Print(md5String(data));
	data := current^.hash + hash + time;

	fname := getUserSaveDir + current^.name;
	assignFile(f, fname);
	rewrite(f);
	for i := 1 to 68 do
		write(f, data[i]);
	closeFile(f);

end;

{ ----------------------------------------------------------------------------
  Procedure to load the best time for a given stage. It uses the hash in the
  file to compare to the hash of the stage and check that the stage is the
  same.

  s  Stage to load the best time.
  ---------------------------------------------------------------------------- }
procedure loadBestTimeFromFile(var s : ph_t_stage);
var
	f     : file of char;
	fname : shortString;
	data  : shortString;
	hash1 : shortString;
	hash2 : shortString;
	time  : shortString;
	error : boolean = false;
	i     : smallint;
begin

	fname := getUserSaveDir + s.name;
	if not fileExists(fname) then exit;

	assignFile(f, fname);
	reset(f);

	hash1[0] := char(32);
	hash2[0] := char(32);
	time [0] := char( 4);
	for i := 1 to 32 do read(f, hash1[i]);
	for i := 1 to 32 do read(f, hash2[i]);
	for i := 1 to  4 do read(f, time [i]);

	if hash1 = s.hash then
	begin
		data := hash1 + time;
		if hash2 = md5Print(md5String(data)) then
		begin
			for i := 0 to 3 do
				t_bytes_4(s.best_time)[i] := time[i + 1];
		end
		else
			error := true;
	end
	else
		error := true;

	if error then
		log_add('Error loading best time for ' + s.name +
		        '. Was the stage edited?');

	closeFile(f);

end;


end.
