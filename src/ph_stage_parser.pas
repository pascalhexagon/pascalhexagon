{ ----------------------------------------------------------------------------
  File: ph_stage_parser.pas

  Unit that implements a parser to read the stage files.
  ----------------------------------------------------------------------------
  Copyright 2016, 2017 Eduardo Mezêncio.

  This file is part of Pascal Hexagon.

  Pascal Hexagon is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Pascal Hexagon is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Pascal Hexagon.  If not, see <http://www.gnu.org/licenses/>.
  ---------------------------------------------------------------------------- }

unit ph_stage_parser;

//=========//
  INTERFACE
//=========//

uses
	ph_stage;


procedure loadStageFromFile(const fname : string; var s : ph_t_stage);


//==============//
  IMPLEMENTATION
//==============//

uses
	sysutils,
	md5,
	{$IFDEF USE_ZENGL_STATIC}
	zgl_log,
	{$ELSE}
	zglHeader,
	{$ENDIF}
	ph_file,
	ph_resources,
	ph_lfo,
	ph_polygon,
	ph_course;

const
	MAX_OBSTACLE_CHARS  = 16;
	ERROR_STATE         = 666;
	READ_SHORTINT_STATE = 667;
	READ_SMALLINT_STATE = 668;
	READ_SINGLE_STATE   = 669;
	FINISH_STATE        = 670;


function  errorLoadingStage       (const error_string : string) : smallint; forward;
procedure fillUnaryWallGroups     (var c : ph_t_course); forward;
procedure fillUnaryGauntletGroups (var c : ph_t_course); forward;
procedure fillGauntletSpaceAfter  (var c : ph_t_course;
                                       i : smallint;
                                       v : single); forward;

{ ----------------------------------------------------------------------------
  Procedure parse a stage file and load the stage into the given stage record.
  The parser is a little complicated, but it's a state machine and each state
  has a comment about what it does.

  fname  Name of the stage file to parse.
  s      Record to receive the stage data.
  ---------------------------------------------------------------------------- }
procedure loadStageFromFile(const fname : string; var s : ph_t_stage);
type
	t_obstacle_char = record
		c : char;
		v : single;
	end;
	t_single_pair = array[0..1] of single;
	t_type = (TYPE_SINGLE, TYPE_DOUBLE, TYPE_COLORS,
	          TYPE_LFOS  , TYPE_STRING);
var
	state   : smallint;
	next    : smallint;
	section : smallint;
	size    : array[0..3] of smallint;
	index   : array[0..3] of smallint;
	depth   : shortint;
	reset_i : boolean;
	chars   : array[0..(MAX_OBSTACLE_CHARS - 1)] of t_obstacle_char;
	chars_c : shortint;
	def_dst : single;
	ptr     : array[0..1] of pointer;
	val     : smallint;
	typ     : t_type;
	lfo     : ph_t_lfo;
	c       : ph_p_course;
	i       : smallint;
begin

	c := @(s.course);

	s.game_speed   := 0;
	s.player_speed := 0;
	for i := 0 to (STAGE_TOTAL_LFO - 1) do
	begin
		s.lfo_count       [i] := 0;
		s.lfo_default_val [i] := 0;
	end;
	for i := 0 to 5 do s.colors[i] := single(i mod 2);
	s.color_swap   := 0;
	s.soundtrack   := -1;

	def_dst := 0;

	openPhFile(fname);
	resetPhFile;

	state   := 0;
	section := 0;
	depth   := 0;

	readChar(false);

	while (not phEof) or (state = FINISH_STATE) do
	begin

		case state of

		0: begin { Reading name or section separator }
			if charIsWhitespace then readChar;
			if charIsLetter then
				state := 1 { Read variable name }
			else if getFileChar = '=' then
			begin
				state := 15; { Finish section }
				readChar;
			end
			else state := errorLoadingStage('expected identifier or section separator.');

		end;

		1: begin { Reading variable name }
			readName;
			case getFileString of
				'gs'   : begin ptr[0] := @(s.game_speed);
				               typ    := TYPE_SINGLE;            end;
				'ps'   : begin ptr[0] := @(s.player_speed);
				               typ    := TYPE_SINGLE;            end;
				'ch'   : begin ptr[0] := @(s.colors[STAGE_COLOR_H0]);
				               typ    := TYPE_COLORS;            end;
				'cs'   : begin ptr[0] := @(s.colors[STAGE_COLOR_S0]);
				               typ    := TYPE_COLORS;            end;
				'cv'   : begin ptr[0] := @(s.colors[STAGE_COLOR_V0]);
				               typ    := TYPE_COLORS;            end;
				'swp'  : begin ptr[0] := @(s.color_swap);
				               typ    := TYPE_DOUBLE;            end;
				'lfox' : begin val    := STAGE_LFO_EXPANSION;
				               typ    := TYPE_LFOS;              end;
				'lfoh' : begin val    := STAGE_LFO_HUE;
				               typ    := TYPE_LFOS;              end;
				'lfos' : begin val    := STAGE_LFO_SAT;
				               typ    := TYPE_LFOS;              end;
				'lfov' : begin val    := STAGE_LFO_VAL;
				               typ    := TYPE_LFOS;              end;
				'lfor' : begin val    := STAGE_LFO_ROTATION;
				               typ    := TYPE_LFOS;              end;
				'lfoa' : begin val    := STAGE_LFO_TILT_ANG;
				               typ    := TYPE_LFOS;              end;
				'lfom' : begin val    := STAGE_LFO_TILT_MAG;
				               typ    := TYPE_LFOS;              end;
				'st'   :       typ    := TYPE_STRING;
			else state := errorLoadingStage('wtf is a ' + getFileString + '?');
			end;
			if state <> ERROR_STATE then
				state := 2; { Read '=' for assignment }
		end;

		2: begin { Reading '=' for assignment }
			if charIsWhitespace then readChar;
			if getFileChar = '=' then
			begin
				case typ of
					TYPE_SINGLE,
					TYPE_DOUBLE  : state := 3;  { Read real value for assignment }
					TYPE_COLORS  : state := 5;  { Read '(' for colors }
					TYPE_LFOS    : state := 8;  (* Read '{' for LFOs or real value *)
					TYPE_STRING  : state := 41; { Read string for soundtrack }
				end;
				readChar;
			end
			else state := errorLoadingStage('expected ''=''.');
		end;

		3: begin { Reading real value for assignment }
			readFloat;
			if stringIsNumber then
			begin
				if      typ = TYPE_SINGLE then PSingle(ptr[0])^ := single(getFileFloat)
				else if typ = TYPE_DOUBLE then PDouble(ptr[0])^ := double(getFileFloat);
				state := 4; { Read ';' }
				next  := 0; { Read name or section separator }
			end
			else state := errorLoadingStage('expected float.');
		end;

		4: begin { Reading ';' }
			if charIsWhitespace then readChar;
			if getFileChar = ';' then
			begin
				state := next;
				readChar;
			end
			else state := errorLoadingStage('expected '';''.');
		end;

		5: begin { Reading '(' for colors }
			if charIsWhitespace then readChar;
			if getFileChar = '(' then
			begin
				index[0] := 0;
				state := 6; { Read color list value }
				readChar;
			end
			else state := errorLoadingStage('expected ''(''.');
		end;

		6: begin { Reading color list value }
			readFloat;
			if stringIsNumber then
			begin
				t_single_pair(ptr[0]^)[index[0]] := single(getFileFloat);
				state := 7; { Read ',' or ')' for colors }
			end
			else state := errorLoadingStage('expected float.');
		end;

		7: begin { Reading ',' or ')' for colors }
			if charIsWhitespace then readChar;
			case getFileChar of
				',': begin
					if index[0] = 1 then
						state := errorLoadingStage('expected only 2 elements.')
					else
					begin
						inc(index[0]);
						state := 6; { Read color list value }
						readChar;
					end;
				end;
				')': begin
					if index[0] = 0 then
						state := errorLoadingStage('expected 2 elements.')
					else
					begin
						state := 4; { Read ';' }
						next  := 0; { Read name or section separator }
						readChar;
					end;
				end;
			else
				if index[0] >= 1 then
					state := errorLoadingStage('expected '')''.')
				else
					state := errorLoadingStage('expected '',''.');
			end;
		end;

		8: begin (* Reading '{' for LFOs or real value *)
			if charIsWhitespace then readChar;
			if charIsNumber(true, true) then
				state := 9   { Read default value for LFO }
			else if getFileChar = '{' then
			begin
				s.lfo_count[val] := 0;
				index[0] :=  0;
				state    := 10; { Read '(' for LFO }
				readChar;
			end
			else state := errorLoadingStage('expected ''{'' or float.');
		end;

		9: begin { Reading default value for LFO }
			readFloat;
			if stringIsNumber then
			begin
				s.lfo_default_val [val] := getFileFloat;
				s.lfo_count       [val] := 0;
				state := 4; { Read ';' }
				next  := 0; { Read name or section separator }
			end
			else state := errorLoadingStage('expected float.');
		end;

		10: begin { Reading '(' for LFO }
			if charIsWhitespace then readChar;
			if getFileChar = '(' then
			begin
				index[1] := 0;
				state := 11; { Read waveshape }
				readChar;
			end
			else state := errorLoadingStage('expected ''(''.');
		end;

		11: begin { Reading waveshape }
			readName;
			if getFileString = '' then
				state := errorLoadingStage('expected wave type.')
			else
			begin
				state := 12; { Read ',' or ')' for LFO parameters }
				case getFileString of
					'sin' : lfo.wave := sin_wave;
					'sqr' : lfo.wave := sqr_wave;
					'saw' : lfo.wave := saw_wave;
					'tri' : lfo.wave := tri_wave;
				else state := errorLoadingStage(getFileString + 'is not a supported wave type.');
				end;
			end;
		end;

		12: begin { Reading ',' or ')' for LFO parameters }
			if charIsWhitespace then readChar;
			case getFileChar of
				',': begin
					inc(index[1]);
					state := 13; { Read LFO real parameter }
					readChar;
				end;
				')': begin
					if (index[1] < 1) or (index[1] > 4) then
						state := errorLoadingStage('expected between 2 and 5 elements.')
					else
					begin
						if index[0] >= STAGE_MAX_LFO then
							state := errorLoadingStage('exceeded maximum lfos. (STAGE_MAX_LFO='
							                            + intToStr(STAGE_MAX_LFO) + ')')
						else
						begin
							inc(s.lfo_count[val]);
							case index[1] of
								1: initLfo(s.lfos[val * STAGE_MAX_LFO + index[0]], lfo.wave, lfo.frequency);
								2: initLfo(s.lfos[val * STAGE_MAX_LFO + index[0]], lfo.wave, lfo.frequency, lfo.amplitude);
								3: initLfo(s.lfos[val * STAGE_MAX_LFO + index[0]], lfo.wave, lfo.frequency, lfo.amplitude, lfo.phase);
								4: initLfo(s.lfos[val * STAGE_MAX_LFO + index[0]], lfo.wave, lfo.frequency, lfo.amplitude, lfo.phase, lfo.offset);
							end;
							state := 14; (* Read ',' or '}' for LFOs *)
							readChar
						end;
					end;
				end;
			else state := errorLoadingStage('expected '','' or '')''.');
			end;
		end;

		13: begin { Reading LFO real parameter }
			readFloat;
			if stringIsNumber then
			begin
				case index[1] of
					1: lfo.frequency := getFileFloat;
					2: lfo.amplitude := getFileFloat;
					3: lfo.phase     := getFileFloat;
					4: lfo.offset    := getFileFloat;
				end;
				state := 12; { Read ',' or ')' for LFO parameters }
			end
			else state := errorLoadingStage('expected float.');
		end;

		14: begin (* Reading ',' or '}' for LFOs *)
			if charIsWhitespace then readChar;
			case getFileChar of
				',': begin
					inc(index[0]);
					state := 10; { Read '(' for LFO }
					readChar;
				end;
				'}': begin
					state := 4; { Read ';' }
					next  := 0; { Read name or section separator }
					readChar;
				end;
			else state := errorLoadingStage('expected '','' or ''}''.');
			end;
		end;

		15: begin { Finish section }
			while getFileChar = '=' do readChar;
			if section = 0 then
			begin
				if s.game_speed <= 0 then
					state := errorLoadingStage('game speed (gs) invalid or not set.');
				if s.player_speed <= 0 then
					state := errorLoadingStage('player speed (ps) invalid or not set.');
				if state <> ERROR_STATE then
				begin
					s.game_speed   := s.game_speed / 1000;
					s.player_speed := s.player_speed * PI / 180;
				end;
			end;
			if state <> ERROR_STATE then
			begin
				inc(section);
				depth    := 0;
				index[0] := 0;
				if section < 6 then state := 16 { Read '[' for array size }
				               else state := FINISH_STATE;
			end;
		end;

		16: begin { Reading '[' for array size }
			if charIsWhitespace then readChar;
			if getFileChar = '[' then
			begin
				case section of
					1:                   ptr[1] := @(c^.walls);
					2: if depth = 0 then ptr[1] := @(c^.wall_groups)
					                else ptr[1] := @(c^.wall_groups[index[1]]);
					3: if depth = 0 then ptr[1] := @(c^.gauntlets)
					                else ptr[1] := @(c^.gauntlets[index[1]]);
					4: if depth = 0 then ptr[1] := @(c^.gauntlet_groups)
					                else ptr[1] := @(c^.gauntlet_groups[index[1]]);
					5: case depth of  0: ptr[1] := @(c^.sequences);
					                  1: ptr[1] := @(c^.sequences[index[1] - c^.min_sides]);
					                  3: ptr[1] := @(c^.sequences[index[1] - c^.min_sides][index[2]]);
					end;
				end;
				state := 17; { Read integer value fot array size }
				readChar;
			end
			else state := errorLoadingStage('expected ''[''.');
		end;

		17: begin { Reading integer value fot array size }
			readInteger;
			if stringIsNumber then
			begin
				if getFileInteger > 0 then
				begin
					case section of
						1: begin setLength(p_wall_array(ptr[1])^, getFileInteger);
						         size[depth + 1] := getFileInteger; end;
						2: if depth = 0 then
						begin
							if getFileInteger < index[1] then
								state := errorLoadingStage('size should be >= ' + intToStr(index[1]) + '.')
							else
							begin
								setLength(p_wall_group_array(ptr[1])^, getFileInteger);
								size[depth + 1] := getFileInteger;
								fillUnaryWallGroups(c^);
							end;
						end
						else
						begin
							setLength(p_smallint_array(ptr[1])^, getFileInteger);
							size[depth] := getFileInteger;
						end;
						3: if depth = 0 then begin setLength(p_gauntlet_array         (ptr[1])^, getFileInteger);
						                           size[depth + 1] := getFileInteger; end
						                else begin setLength(p_gauntlet_element_array (ptr[1])^, getFileInteger);
						                           size[depth] := getFileInteger; end;
						4: if depth = 0 then
						begin
							if getFileInteger < index[1] then
								state := errorLoadingStage('size should be >= ' + intToStr(index[1]) + '.')
							else
							begin
								setLength(p_gauntlet_group_array(ptr[1])^, getFileInteger);
								size[depth + 1] := getFileInteger;
								fillUnaryGauntletGroups(c^);
							end;
						end
						else
						begin
							setLength(p_shortint_array(ptr[1])^, getFileInteger);
							size[depth] := getFileInteger;
						end;
						5: case depth of
							0: begin c^.max_sides := getFileInteger - 1;
						             setLength(p_sequence_array_array (ptr[1])^, getFileInteger);
						             size[depth + 1] := getFileInteger; end;
						    1: begin setLength(p_sequence_array       (ptr[1])^, getFileInteger);
						             size[depth + 1] := getFileInteger; end;
						    3: begin setLength(p_shortint_array       (ptr[1])^, getFileInteger);
						             size[depth] := getFileInteger; end;
						end;
					end;
					if state <> ERROR_STATE then
						state := 18; { Read ']' for array size }
				end
				else state := errorLoadingStage('size should be > 0.');
			end
			else state := errorLoadingStage('expected integer.');
		end;

		18: begin { Reading ']' for array size }
			if charIsWhitespace then readChar;
			if getFileChar = ']' then
			begin
				case section of
					1:                          state   := 19;  { Read '(' for parameter list }
					2: if depth = 0  then begin reset_i := false;
					                            state   := 26;  (* Read '{' for scope *)
					                            next    := 27;  { Read index } end
					                 else begin ptr[0]  := @(c^.wall_groups[index[1]][0]);
					                            state   := READ_SMALLINT_STATE;
					                            next    := 33;  { Read ',' or ';' } end;
					3: if depth = 0  then       state   := 19   { Read '(' for parameter list }
					                 else begin fillGauntletSpaceAfter(c^, index[1], def_dst);
					                 			ptr[0]  := @(c^.gauntlets[index[1]][0].wall_group);
					                            state   := READ_SMALLINT_STATE;
					                            next    := 35;  { Read '|', '[', ',' or ';' } end;
					4: if depth = 0  then begin reset_i := false;
					                            state   := 26;  (* Read '{' for scope *)
					                            next    := 27;  { Read index } end
					                 else begin ptr[0]  := @(c^.gauntlet_groups[index[1]][0]);
					                            state   := READ_SHORTINT_STATE;
					                            next    := 33;  { Read ',' or ';' } end;
					5: case depth of 0:         state   := 19;  { Read '(' for parameter list }
					                 1:   begin reset_i := true;
					                            state   := 26;  (* Read '{' for scope *)
					                            next    := 27;  { Read index } end;
					                 3:   begin ptr[0]  := @(c^.sequences[index[1] - c^.min_sides][index[2]][0]);
					                            state   := READ_SHORTINT_STATE;
					                            next    := 33;  { Read ',' or ';' } end;
					   end;
				end;
				readChar;
			end
			else state := errorLoadingStage('expected '']''.');
		end;

		19: begin { Reading '(' for parameter list }
			if charIsWhitespace then readChar;
			if getFileChar = '(' then
			begin
				case section of
					1:       state  := 20; { Read '(' for obstacle char }
					3: begin ptr[0] := @(def_dst);
					         state  := READ_SINGLE_STATE;
					         next   := 25; { Read ',' or ')' for parameter list } end;
					5: begin ptr[0] := @(c^.min_sides);
					         state  := READ_SHORTINT_STATE;
					         next   := 25; { Read ',' or ')' for parameter list } end;
				end;
				readChar;
			end
			else state := errorLoadingStage('expected ''(''.');
		end;

		20: begin { Reading '(' for obstacle char }
			if charIsWhitespace then readChar;
			if getFileChar = '(' then
			begin
				state := 21; { Read quotes for char }
				next  := 22; { Read char }
				readChar;
			end
			else state := errorLoadingStage('expected ''(''.');
		end;

		21: begin { Reading quotes for char }
			if charIsWhitespace then readChar;
			if getFileChar = char(39) then
			begin
				state := next;
				readChar;
			end
			else state := errorLoadingStage('expected apostrophe quote.');
		end;

		22: begin { Reading char }
			if charIsWhitespace then readChar;
			chars[index[0]].c := getFileChar;
			state := 21; { Read quotes for char }
			next  := 23; { Read ',' for obstacle char }
			readChar;
		end;

		23: begin { Reading ',' for obstacle char }
			if charIsWhitespace then readChar;
			if getFileChar = ',' then
			begin
				ptr[0] := @(chars[index[0]].v);
				state := READ_SINGLE_STATE;
				next  := 24; { Read ')' for obstacle char }
				readChar;
			end
			else state := errorLoadingStage('expected '',''.');
		end;

		24: begin { Reading ')' for obstacle char }
			if charIsWhitespace then readChar;
			if getFileChar = ')' then
			begin
				state := 25; { Read ',' or ')' for parameter list }
				readChar;
			end
			else state := errorLoadingStage('expected '')''.');
		end;

		25: begin { Reading ',' or ')' for parameter list }
			if charIsWhitespace then readChar;
			if getFileChar = ',' then
			begin
				inc(index[0]);
				case section of
					1:       state    := 20; { Read '(' for obstacle char }
					3:       state    := errorLoadingStage('expected '')''.');
					5: begin inc(c^.max_sides, c^.min_sides);
					         ptr[0]   := @(c^.init_sides);
					         index[1] := c^.min_sides;
					         state    := READ_SHORTINT_STATE;
					         next     := 25; { Read ',' or ')' for parameter list } end;
				end;
				readChar;
			end
			else if getFileChar = ')' then
			begin
				case section of
					1: begin
						if index[0] >= MAX_OBSTACLE_CHARS then
							state := errorLoadingStage('exceeded maximum obstacle chars. (' + intToStr(MAX_OBSTACLE_CHARS) + ')')
						else
						begin
							chars_c := index[0] + 1;
							reset_i := true;
						end;
					end;
					3: reset_i := true;
					5: if index[0] >= 2 then state := errorLoadingStage('expected only 2 parameters.')
						                else reset_i := false;
				end;
				if state <> ERROR_STATE then
				begin
					state := 26;  (* Read '{' for scope *)
					next  := 27;  { Read index }
					readChar;
				end;
			end
			else state := errorLoadingStage('expected '','' or '')''.');
		end;

		26: begin (* Reading '{' for scope *)
			if charIsWhitespace then readChar;
			if getFileChar = '{' then
			begin
				inc(depth);
				if reset_i then index[depth] := 0;
				state := next;
				readChar;
			end
			else state := errorLoadingStage('expected ''{''.');
		end;

		27: begin { Reading index }
			readInteger;
			if stringIsNumber then
			begin
				if getFileInteger = index[depth] then
					state := 28 { Read ':' for index }
				else state := errorLoadingStage('expected index ' + intToStr(index[depth]) + '.');
			end
			else state := errorLoadingStage('expected integer index.');
		end;

		28: begin { Reading ':' for index }
			if charIsWhitespace then readChar;
			if getFileChar = ':' then
			begin
				if (section <> 5) or ((section = 5) and (depth = 2)) then
				begin
					inc(depth);
					index[depth] := 0;
				end;
				if section = 1 then state := 29  { Read obstacle char or ';' }
				               else state := 16; { Read '[' for array size }
				readChar;
			end
			else state := errorLoadingStage('expected '':''.');
		end;

		29: begin { Reading obstacle char or ';' }
			if charIsWhitespace then readChar;
			if getFileChar = ';' then
			begin
				if index[2] < MAX_POLYGON then
					for i := index[2] to (MAX_POLYGON - 1) do
						c^.walls[index[1]][index[2]] := 0;
				dec(depth);
				inc(index[depth]);
				if index[depth] = size[depth] then
				begin
					state := 30; (* Read '}' for scope *)
					next  := 31; { Read sub-section separator }
				end
				else
					state := 27; { Read index }
				readChar;
			end
			else
			begin
				for i := 0 to (chars_c) do
				begin
					if i = chars_c then
						state := errorLoadingStage('character ''' + getFileChar + ''' not defined.')
					else if getFileChar = chars[i].c then
					begin
						if index[2] < MAX_POLYGON then
						begin
							c^.walls[index[1]][index[2]] := chars[i].v;
							inc(index[2]);
							readChar;
							break;
						end
						else state := errorLoadingStage('exceeded maximum polygon sides. (MAX_POLYGON=' + intToStr(MAX_POLYGON) + ')');
					end;
				end;
			end;
		end;

		30: begin (* Reading '}' for scope *)
			if charIsWhitespace then readChar;
			if getFileChar = '}' then
			begin
				repeat
					dec(depth);
					readChar;
				until getFileChar <> '}';
				inc(index[depth]);
				state := next;
			end
			else state := errorLoadingStage('expected ''}''.');
		end;

		31: begin { Reading sub-section separator }
			if charIsWhitespace then readChar;
			if getFileChar = '-' then
			begin
				state := 32; { Finish sub-section }
				readChar;
			end
			else state := errorLoadingStage('expected subsection separator.');
		end;

		32: begin { Finishing sub-section }
			while getFileChar = '-' do readChar;
			inc(section);
			depth := 0;
			state := 16; { Read '[' for array size }
		end;

		33: begin { Reading ',' or ';' }
			if charIsWhitespace then readChar;
			if getFileChar = ',' then
			begin
				inc(index[depth]);
				if index[depth] >= size[depth] then
					state := errorLoadingStage('exceeded array size.')
				else
				begin
					case section of 2: ptr[0] := @(c^.wall_groups     [index[1]][index[2]]);
						            3: ptr[0] := @(c^.gauntlets       [index[1]][index[2]].wall_group);
						            4: ptr[0] := @(c^.gauntlet_groups [index[1]][index[2]]);
						            5: ptr[0] := @(c^.sequences       [index[1] - c^.min_sides][index[2]][index[3]]);
					end;
					case section of 2, 3: state := READ_SMALLINT_STATE;
						            4, 5: state := READ_SHORTINT_STATE;
					end;
					if section = 3 then next := 35  {ler '|', '[', ',' ou ';'}
						           else next := 33; {ler ',' ou ';'}
					readChar;
				end;
			end
			else if getFileChar = ';' then
			begin
				inc(index[depth]);
				if index[depth] = size[depth] then
				begin
					dec(depth);
					inc(index[depth]);
					if index[depth] = size[depth] then
					begin
						state := 30; (* Read '}' for scope *)
						case section of
							2, 4: next := 34; { Read section separator }
							3:    next := 31; { Read sub-section separator }
							5: if index[1] = c^.max_sides then next := 34  { Read section separator }
							                              else next := 27; { Read sub-section separator }
						end;
					end
					else
						state := 27; { Read index }
					readChar;
				end
				else state := errorLoadingStage('expected '',''.');
			end
			else state := errorLoadingStage('expected ''}''.');
		end;

		34: begin { Reading section separator }
			if charIsWhitespace then readChar;
			if getFileChar = '=' then
			begin
				state := 15; { Finish section }
				readChar;
			end
			else state := errorLoadingStage('expected section separator.');
		end;

		35: begin { Reading '|', '[', ',' or ';' }
			if charIsWhitespace then readChar;
			case getFileChar of
				'|': begin state := 36; { Read distance multiplier }
				           readChar; end;
				'[': begin state := 39; { Read element multiplier }
				           readChar; end;
				',', ';':  state := 33; { Read ',' or ';' }
			else state := errorLoadingStage('expected ''|'', ''['', '','' or '';''.');
			end;
		end;

		36: begin { Reading distance multiplier }
			readFloat;
			if stringIsNumber then
			begin
				if getFileFloat > 0 then
				begin
					c^.gauntlets[index[1]][index[2]].space_after := def_dst * getFileFloat;
					state := 37; { Read '|' }
				end
				else state := errorLoadingStage('multiplier should be > 0.');
			end
			else state := errorLoadingStage('expected float.');
		end;

		37: begin { Reading '|' }
			if charIsWhitespace then readChar;
			if getFileChar = '|' then
			begin
				state := 38; { Read '[', ',' or ';' }
				readChar;
			end
			else state := errorLoadingStage('expected ''|''.');
		end;

		38: begin { Reading '[', ',' ou ';' }
			if charIsWhitespace then readChar;
			case getFileChar of
				'[': begin state := 39; { Read element multiplier }
				           readChar; end;
				',', ';':  state := 33; { Read ',' or ';' }
			else state := errorLoadingStage('expected ''['', '','' or '';''.');
			end;
		end;

		39: begin { Reading element multiplier }
			readInteger;
			if stringIsNumber then
			begin
				if getFileInteger > 0 then
				begin
					if (index[2] + getFileInteger - 1) < size[2] then
					begin
						for i := 1 to (getFileInteger - 1) do
						begin
							inc(index[2]);
							c^.gauntlets[index[1]][index[2]] := c^.gauntlets[index[1]][index[2] - 1];
						end;
						state := 40; { Read ']' }
					end
					else state := errorLoadingStage('exceeded array size.');
				end
				else state := errorLoadingStage('multiplier should be > 0.');
			end
			else state := errorLoadingStage('expected integer.');
		end;

		40: begin { Reading ']' }
			if charIsWhitespace then readChar;
			if getFileChar = ']' then
			begin
				state := 33; { Read ',' or ';' }
				readChar;
			end
			else state := errorLoadingStage('expected '']''.');
		end;

		41: begin { Reading string for soundtrack }
			readName;
			if getFileString <> '' then
			begin
				s.soundtrack := getMusicIndex(getFileString);
				state := 4; { Read ';' }
				next  := 0; { Read name or section separator }
			end
			else state := errorLoadingStage('expected music identifier.');
		end;

		ERROR_STATE: begin
			s.error := true;
			break;
		end;

		READ_SHORTINT_STATE: begin
			readInteger;
			if stringIsNumber then
			begin
				PShortint(ptr[0])^ := shortint(getFileInteger);
				state := next;
			end
			else state := errorLoadingStage('expected integer.');
		end;

		READ_SMALLINT_STATE: begin
			readInteger;
			if stringIsNumber then
			begin
				PSmallint(ptr[0])^ := smallint(getFileInteger);
				state := next;
			end
			else state := errorLoadingStage('expected integer.');
		end;

		READ_SINGLE_STATE: begin
			readFloat;
			if stringIsNumber then
			begin
				PSingle(ptr[0])^ := single(getFileFloat);
				state := next;
			end
			else state := errorLoadingStage('expected float.');
		end;

		FINISH_STATE: begin
			if charIsWhitespace then readChar;
			if phEof then
			begin
				log_add('pascalhexagon: Loaded stage ' + s.name + ' successfully.');
				break;
			end
			else
				state := errorLoadingStage('expected EOF.');
		end;

		end;

	end;

	closePhFile;

	if not s.error then
		s.hash := MD5Print(MD5File(fname));

end;

{ ----------------------------------------------------------------------------
  Function to report errors from the parser, including the place where the
  error happened and a message describing the error.

  error_string  String with the error description.

  return        Returns ERROR_STATE to be used by the parser.
  ---------------------------------------------------------------------------- }
function errorLoadingStage(const error_string : string) : smallint;
begin

	log_add('pascalhexagon: ' + fileErrorMessage + error_string);
	errorLoadingStage := ERROR_STATE;

end;

{ ----------------------------------------------------------------------------
  Procedure to fill in the wall groups that represent each of the walls alone.
  The game always uses wall groups and gauntlet groups, instead of walls and
  gauntlets, so for each wall and gauntlet there is a group that contains it
  alone.
  ---------------------------------------------------------------------------- }
procedure fillUnaryWallGroups(var c : ph_t_course);
var
	i : smallint;
begin

	for i := 0 to (length(c.walls) - 1) do
	begin
		setLength(c.wall_groups[i], 1);
		c.wall_groups[i][0] := i;
	end;

end;

{ ----------------------------------------------------------------------------
  Procedure to fill in the gauntlet groups that represent each of the
  gauntlets alone. The game always uses wall groups and gauntlet groups,
  instead of walls and gauntlets, so for each wall and gauntlet there is a
  group that contains it alone.
  ---------------------------------------------------------------------------- }
procedure fillUnaryGauntletGroups(var c : ph_t_course);
var
	i : smallint;
begin

	for i := 0 to (length(c.gauntlets) - 1) do
	begin
		setLength(c.gauntlet_groups[i], 1);
		c.gauntlet_groups[i][0] := i;
	end;

end;

{ ----------------------------------------------------------------------------
  Procedure to fill the space_after of all walls in a gauntlet with a value.

  c       Course being changed.
  i       Index of the gauntlet to change.
  v       Value to set the space_after.
  ---------------------------------------------------------------------------- }
procedure fillGauntletSpaceAfter(var c : ph_t_course; i : smallint; v : single);
var
	j : smallint;
begin

	for j := 0 to (length(c.gauntlets[i]) - 1) do
		c.gauntlets[i][j].space_after := v;

end;

end.
