{ ----------------------------------------------------------------------------
  File: ph_player.pas

  Unit that implements an interface for the game to control, draw and get
  information about the player.
  ----------------------------------------------------------------------------
  Copyright 2016, 2017 Eduardo Mezêncio.

  This file is part of Pascal Hexagon.

  Pascal Hexagon is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Pascal Hexagon is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Pascal Hexagon.  If not, see <http://www.gnu.org/licenses/>.
  ---------------------------------------------------------------------------- }

unit ph_player;

//=========//
  INTERFACE
//=========//

uses
	{$IFDEF USE_ZENGL_STATIC}
	zgl_fx,
	zgl_primitives_2d,
	{$ELSE}
	zglHeader,
	{$ENDIF}
	ph_global,
	ph_colors,
	ph_camera;

const
	PLAYER_DIST = 1;

type
	ph_t_direction = (center, left, right);

function  getPlayerAngle     () : single;

procedure resetPlayerAngle   ();
procedure movePlayerTo       (dest : single);
procedure setPlayerDirection (d : ph_t_direction);

procedure updatePlayer       ();
procedure drawPlayer         ();

//==============//
  IMPLEMENTATION
//==============//

uses
	ph_helper,
	ph_polygon;

const
	PLAYER_SIZE     = 6;
	DIRECTION_ANGLE = PI / 5;
	DIRECTION_SPEED = PI / 64;

var
	tl_player     : array[0..2] of TPoint;
	tl_player_t   : array[0..2] of TPoint;

	translation   : single;
	rotation      : single;
	rotation_dest : single;


{ ----------------------------------------------------------------------------
  Function to return the player's position (angle in radians).

  return  Returns the player's position (angle in radians).
  ---------------------------------------------------------------------------- }
function getPlayerAngle() : single;
begin

	getPlayerAngle := translation;

end;

{ ----------------------------------------------------------------------------
  Procedure to reset the player to the starting position.
  ---------------------------------------------------------------------------- }
procedure resetPlayerAngle();
begin

	translation   := 0;
	rotation      := 0;
	rotation_dest := 0;

end;

{ ----------------------------------------------------------------------------
  Procedure to move the player to a given position (angle in radians).

  dest  Position (angle in radians) to move the player.
  ---------------------------------------------------------------------------- }
procedure movePlayerTo(dest : single);
begin

	translation := dest;

end;

{ ----------------------------------------------------------------------------
  Procedure to set the direction the player is facing, so the player's
  triangle is animated correctly.

  d  Direction the player is facing.
  ---------------------------------------------------------------------------- }
procedure setPlayerDirection (d : ph_t_direction);
begin

	case d of
		center : rotation_dest :=  0;
		left   : rotation_dest := -DIRECTION_ANGLE;
		right  : rotation_dest :=  DIRECTION_ANGLE;
	end;

end;

{ ----------------------------------------------------------------------------
  Procedure to update the player's geometry. Should be called every frame
  before drawing the player.
  ---------------------------------------------------------------------------- }
procedure updatePlayer();
var
	i    : integer;
	size : single;
	dist : single;
begin

	size := PLAYER_SIZE * Z_DISTANCE * getScreenMeanRatio / getCameraZoom;
	dist := PLAYER_DIST + getPolygonExpansion / PLAYER_DIST;

	moveTowards(rotation, rotation_dest, DIRECTION_SPEED);

	for i := 0 to 2 do
	begin
		tl_player[i].x := size * cos(i * pi * 2/3 + rotation) + dist;
		tl_player[i].y := size * sin(i * pi * 2/3 + rotation);

		tl_player_t[i].x :=   tl_player[i].x * cos(translation)
		                    - tl_player[i].y * sin(translation);
		tl_player_t[i].y :=   tl_player[i].x * sin(translation)
		                    + tl_player[i].y * cos(translation);

		projectPoint(tl_player_t[i]);
	end;

end;

{ ----------------------------------------------------------------------------
  Procedure to draw the player.
  ---------------------------------------------------------------------------- }
procedure drawPlayer();
begin

	pr2d_TriList( nil, @tl_player_t, nil        ,
	              0  , 2           , getColor(0),
	              255, PR2D_FILL or FX_BLEND   );

end;

end.
