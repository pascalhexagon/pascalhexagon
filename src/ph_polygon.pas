{ ----------------------------------------------------------------------------
  File: ph_player.pas

  Unit that implements an interface for the game to control, draw and get
  information from the polygon. The polygon is the entire background of the
  game, including the tracks where the obstacles move, called 'slices' in the
  code.
  ----------------------------------------------------------------------------
  Copyright 2016, 2017 Eduardo Mezêncio.

  This file is part of Pascal Hexagon.

  Pascal Hexagon is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Pascal Hexagon is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Pascal Hexagon.  If not, see <http://www.gnu.org/licenses/>.
  ---------------------------------------------------------------------------- }

unit ph_polygon;

//=========//
  INTERFACE
//=========//

const
	MAX_POLYGON                    = 6;
	POLYGON_RADIUS                 = 0.9;
	POLYGON_ANIM_PAUSE_DURATION    = 333;
	POLYGON_ANIM_DURATION	       = 500;
	POLYGON_ANIM_SPEED             = 1 / POLYGON_ANIM_DURATION;
	POLYGON_OUTLINE_THICKNESS      = 4;


function  getSliceAngle       (index : shortint) : single;
function  getPolygonSides     ()                 : shortint;
function  getPolygonApothem   ()                 : single;
function  getPolygonExpansion ()                 : single;
function  sliceAt             (angle : single)   : shortint;
function  polygonChanging     ()                 : boolean;

procedure setPolygonExpansion (val   : single);
procedure setPolygonAnimPause (pause : boolean);
procedure changePolygon       (sides : shortint);

procedure resetPolygon        (s     : shortint = MAX_POLYGON);
procedure updatePolygon       (dt    : double);
procedure drawPolygon         ();

//==============//
  IMPLEMENTATION
//==============//

uses
	math,
	{$IFDEF USE_ZENGL_STATIC}
	zgl_fx,
	zgl_primitives_2d,
	{$ELSE}
	zglHeader,
	{$ENDIF}
	ph_global,
	ph_colors,
	ph_camera;

type
	ph_t_polygon_anim_state = (POLYGON_ANIM_OFF,
	                           POLYGON_ANIM_BEFORE,
	                           POLYGON_ANIM_PRE_PAUSE,
	                           POLYGON_ANIM_TRANSFORM,
	                           POLYGON_ANIM_POST_PAUSE);

var
	sides_i            : shortint;
	sides_r            : single;
	apothem            : single;
	anim_pause         : boolean;
	anim_state         : ph_t_polygon_anim_state;
	anim_timer         : double;
	anim_direction     : single;
	slice_angle        : array[0..MAX_POLYGON] of single;
	expansion          : single;

	tl_background_a    : array[0..(3 * (MAX_POLYGON div 2) - 1)] of TPoint;
	tl_background_b    : array[0..(3 * (MAX_POLYGON div 2) - 1)] of TPoint;
	tl_background_c    : array[0..2]                             of TPoint;
	tl_polygon_fill    : array[0..(3 * MAX_POLYGON - 1)]         of TPoint;
	tl_polygon_outline : array[0..(6 * MAX_POLYGON - 1)]         of TPoint;


procedure updateAngles     (); forward;
procedure updateBackground (); forward;
procedure updateOutline    (); forward;


{ ----------------------------------------------------------------------------
  Function to return the angle (in radians) where a given slice starts.

  index   Index of the slice (between 0 and MAX_POLYGON)

  return  Returns the angle (in radians) where the slice starts.
  ---------------------------------------------------------------------------- }
function getSliceAngle(index : shortint) : single;
begin

	getSliceAngle := slice_angle[index];

end;

{ ----------------------------------------------------------------------------
  Function to return the current number of sides of the polygon.

  return  Returns the number of sides of the polygon.
  ---------------------------------------------------------------------------- }
function getPolygonSides() : shortint;
begin

	getPolygonSides := sides_i;

end;

{ ----------------------------------------------------------------------------
  Function to return the current measure of the apothem of the polygon.

  return  Returns the apothem of the polygon.
  ---------------------------------------------------------------------------- }
function getPolygonApothem() : single;
begin

	getPolygonApothem := apothem;

end;

{ ----------------------------------------------------------------------------
  Function to return the current expansion of the polygon. The expansion is
  used to animate the polygon with a speaker-like effect.

  return  Returns the expansion of the polygon.
  ---------------------------------------------------------------------------- }
function getPolygonExpansion() : single;
begin

	getPolygonExpansion := expansion;

end;

{ ----------------------------------------------------------------------------
  Function to return the index of the slice at the given angle (in radians
  with 0 <= angle < 2*pi).

  angle   Angle (in radians with 0 <= angle < 2*pi).

  return  Returns the index of the slice at that angle.
  ---------------------------------------------------------------------------- }
function sliceAt(angle : single) : shortint;
var
	i : integer;
begin

	i := 1;

	while angle > slice_angle[i] do
		inc(i);

	sliceAt := i - 1;

end;

{ ----------------------------------------------------------------------------
  Function to check whether the polygon is in the middle of a transformation,
  changing the number of sides.

  return  Returns whether the polygon is transforming.
  ---------------------------------------------------------------------------- }
function polygonChanging() : boolean;
begin

	polygonChanging := (anim_state >= POLYGON_ANIM_PRE_PAUSE ) and
	                   (anim_state <= POLYGON_ANIM_POST_PAUSE)

end;

{ ----------------------------------------------------------------------------
  Procedure to set the polygon expansion to the given value.

  val  New value for the expansion.
  ---------------------------------------------------------------------------- }
procedure setPolygonExpansion(val : single);
begin

	expansion := val;

end;

{ ----------------------------------------------------------------------------
  Procedure to set whether the polygon animation is paused.

  pause  New boolean value of the polygon animation pause.
  ---------------------------------------------------------------------------- }
procedure setPolygonAnimPause (pause : boolean);
begin

	anim_pause := pause;

end;

{ ----------------------------------------------------------------------------
  Procedure to change the number of sides of the polygon.

  sides  New number of sides of the polygon.
  ---------------------------------------------------------------------------- }
procedure changePolygon(sides : shortint);
begin

	if (sides <= MAX_POLYGON) and (sides >= 3) then
	begin
		sides_i := sides;
		apothem := POLYGON_RADIUS * cos(PI / sides_i);
	end;

end;

{ ----------------------------------------------------------------------------
  Procedure to reset the polygon with the given number of sides.

  s  Number of sides of the polygon.
  ---------------------------------------------------------------------------- }
procedure resetPolygon(s : shortint);
var
	i : shortint;
begin

	sides_i        := s;
	sides_r        := sides_i;
	apothem        := POLYGON_RADIUS * cos(PI / sides_i);
	anim_state     := POLYGON_ANIM_OFF;
	anim_timer     := 0;
	anim_direction := 0;
	expansion      := 0;

	for i := 0 to (ceil(sides_r) div 2 - 1) do
	begin
		tl_background_a[3 * i].x := 0;
		tl_background_a[3 * i].y := 0;
		tl_background_b[3 * i].x := 0;
		tl_background_b[3 * i].y := 0;
	end;
	tl_background_c[0].x := 0;
	tl_background_c[0].y := 0;
	tl_background_c[1].x := 32;
	tl_background_c[1].y := 0;
	for i := 0 to (ceil(sides_r) - 1) do
	begin
		tl_polygon_fill[3 * i].x := 0;
		tl_polygon_fill[3 * i].y := 0;
	end;

	updateAngles;
	updateBackground;
	updateOutline;

end;

{ ----------------------------------------------------------------------------
  Procedure to update the geometry of the polygon. Should be called every
  frame before drawing the polygon, if the polygon is animated.

  dt  Time elapsed since last frame (in milliseconds).
  ---------------------------------------------------------------------------- }
procedure updatePolygon(dt : double);
begin

	case anim_state of

		POLYGON_ANIM_OFF:
		begin
			if sides_r <> sides_i then
			begin
				anim_direction := sides_i - sides_r;
				anim_state := POLYGON_ANIM_BEFORE;
			end;
		end;

		POLYGON_ANIM_BEFORE:
		begin
			if anim_pause then
			begin
				anim_timer := POLYGON_ANIM_PAUSE_DURATION;
				anim_state := POLYGON_ANIM_PRE_PAUSE;
			end
			else
				anim_state := POLYGON_ANIM_TRANSFORM;
		end;

		POLYGON_ANIM_PRE_PAUSE:
		begin
			if anim_timer > 0 then
				anim_timer := anim_timer - dt
			else
			begin
				anim_timer := 0;
				anim_state := POLYGON_ANIM_TRANSFORM;
			end;
		end;

		POLYGON_ANIM_TRANSFORM:
		begin
			sides_r := sides_r + anim_direction * dt * POLYGON_ANIM_SPEED;

			if (anim_direction * sides_r) > (anim_direction * sides_i) then
			begin
				sides_r := sides_i;
				if anim_pause then
				begin
					anim_timer := POLYGON_ANIM_PAUSE_DURATION;
					anim_state := POLYGON_ANIM_POST_PAUSE;
				end
				else
					anim_state := POLYGON_ANIM_OFF;
			end;
			updateAngles;
		end;

		POLYGON_ANIM_POST_PAUSE:
		begin
			if anim_timer > 0 then
				anim_timer := anim_timer - dt
			else
			begin
				anim_timer := 0;
				anim_state := POLYGON_ANIM_OFF;
			end;
		end;

	end;

	updateBackground;
	updateOutline;

end;

{ ----------------------------------------------------------------------------
  Procedure to draw the polygon.
  ---------------------------------------------------------------------------- }
procedure drawPolygon();
var
	sides, vertexes_a, vertexes_b : integer;
begin

	sides      := ceil(sides_r);
	vertexes_a := sides div 2 * 3 - 1;
	vertexes_b := sides mod 2 * 3 - 1;

	pr2d_TriList( nil, @tl_background_a[0]    , nil              ,
	              0  , vertexes_a             , getColor(1),
	              255, PR2D_FILL or FX_BLEND  );

	pr2d_TriList( nil, @tl_background_b[0]    , nil              ,
	              0  , vertexes_a             , getColor(3),
	              255, PR2D_FILL or FX_BLEND  );

	if vertexes_b > 0 then
		pr2d_TriList( nil, @tl_background_c[0]   , nil        ,
		              0  , vertexes_b            , getColor(2),
		              255, PR2D_FILL or FX_BLEND );

	pr2d_TriList( nil, @tl_polygon_fill[0]    , nil        ,
	              0  , sides * 3 - 1          , getColor(3, false),
	              255, PR2D_FILL or FX_BLEND  );

	pr2d_TriList( nil, @tl_polygon_outline[0] , nil        ,
	              0  , sides * 6 - 1          , getColor(0),
	              255, PR2D_FILL or FX_BLEND  );

end;

{ ----------------------------------------------------------------------------
  Procedure to update the angles of the polygon while it's changing the number
  of sides.
  ---------------------------------------------------------------------------- }
procedure updateAngles();
var
	i           : integer;
	slice_width : real;
begin

	slice_width := 2 * pi / sides_r;

	for i := 0 to (ceil(sides_r) - 1) do
	begin
		slice_angle[i] := i * slice_width;
	end;

	slice_angle[ceil(sides_r)] := 2 * pi;

end;

{ ----------------------------------------------------------------------------
  Procedure to update the geometry of the background.
  ---------------------------------------------------------------------------- }
procedure updateBackground();
var
	i, i2, i3 : integer;
	dist      : single;
begin

	for i := 0 to (ceil(sides_r) div 2 - 1) do
	begin
		i2 := 2 * i;
		i3 := 3 * i;

		if i = 0 then
		begin
			tl_background_a[i3 + 1].x := 32 * cos(slice_angle[i2]);
			tl_background_a[i3 + 1].y := 32 * sin(slice_angle[i2]);
		end
		else
			tl_background_a[i3 + 1] := tl_background_b[i3 -1];

		tl_background_a[i3 + 2].x := 32 * cos(slice_angle[i2 + 1]);
		tl_background_a[i3 + 2].y := 32 * sin(slice_angle[i2 + 1]);
		tl_background_b[i3 + 1] := tl_background_a[i3 + 2];
		tl_background_b[i3 + 2].x := 32 * cos(slice_angle[i2 + 2]);
		tl_background_b[i3 + 2].y := 32 * sin(slice_angle[i2 + 2]);
	end;

	if (ceil(sides_r) mod 2) = 1 then
	begin
		tl_background_c[2].x := 32 * cos(slice_angle[ceil(sides_r) - 1]);
		tl_background_c[2].y := 32 * sin(slice_angle[ceil(sides_r) - 1]);
	end;

	dist := POLYGON_RADIUS + expansion / POLYGON_RADIUS;

	for i := 0 to (ceil(sides_r) - 1) do
	begin
		i2 := 3 * i;
		i3 := 6 * i;

		if i = 0 then
		begin
			tl_polygon_fill[i2 + 1].x := dist * cos(slice_angle[i]);
			tl_polygon_fill[i2 + 1].y := dist * sin(slice_angle[i]);
			projectPoint(tl_polygon_fill[i2 + 1]);
		end
		else
			tl_polygon_fill[i2 + 1] := tl_polygon_fill[i2 - 1];

		tl_polygon_fill[i2 + 2].x := dist * cos(slice_angle[i + 1]);
		tl_polygon_fill[i2 + 2].y := dist * sin(slice_angle[i + 1]);
		projectPoint(tl_polygon_fill[i2 + 2]);

		tl_polygon_outline[i3    ].x := dist * cos(slice_angle[i]);
		tl_polygon_outline[i3    ].y := dist * sin(slice_angle[i]);
		tl_polygon_outline[i3 + 1].x := dist * cos(slice_angle[i + 1]);
		tl_polygon_outline[i3 + 1].y := dist * sin(slice_angle[i + 1]);
		projectPoint(tl_polygon_outline[i3]);
		projectPoint(tl_polygon_outline[i3 + 1]);
		tl_polygon_outline[i3 + 3] := tl_polygon_outline[i3 + 1];
	end;

end;

{ ----------------------------------------------------------------------------
  Procedure to update the geometry of the polygon outline.
  ---------------------------------------------------------------------------- }
procedure updateOutline();
var
	i, i2   : integer;
	inner_r : single;
	dist    : single;
begin

	inner_r := POLYGON_RADIUS - POLYGON_OUTLINE_THICKNESS * Z_DISTANCE
	                            * getScreenMeanRatio / getCameraZoom;
	dist := inner_r + expansion / inner_r;

	for i := 0 to (ceil(sides_r) - 1) do
	begin
		i2 := 6 * i;

		tl_polygon_outline[i2 + 2].x := dist * cos(slice_angle[i]);
		tl_polygon_outline[i2 + 2].y := dist * sin(slice_angle[i]);
		tl_polygon_outline[i2 + 5].x := dist * cos(slice_angle[i + 1]);
		tl_polygon_outline[i2 + 5].y := dist * sin(slice_angle[i + 1]);
		projectPoint(tl_polygon_outline[i2 + 2]);
		projectPoint(tl_polygon_outline[i2 + 5]);
		tl_polygon_outline[i2 + 4] := tl_polygon_outline[i2 + 2];
	end;

end;

end.
