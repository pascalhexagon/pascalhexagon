{ ----------------------------------------------------------------------------
  File: ph_obstacles.pas

  Unit that implements an interface for the game to create, check collision
  with, update and draw obstacles. The obstacles are implemented in this file
  as a queue where new obstacles go in the back of the queue and, on update,
  obstacles that passed the side of the polygon are removed from the front of
  the queue.
  ----------------------------------------------------------------------------
  Copyright 2016, 2017 Eduardo Mezêncio.

  This file is part of Pascal Hexagon.

  Pascal Hexagon is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Pascal Hexagon is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Pascal Hexagon.  If not, see <http://www.gnu.org/licenses/>.
  ---------------------------------------------------------------------------- }

unit ph_obstacles;

//=========//
  INTERFACE
//=========//

uses
	ph_global;

type
	ph_p_obstacle = ^ph_t_obstacle;
	{ ------------------------------------------------------------------------
	  Record that represents an obstacle.
	  ------------------------------------------------------------------------ }
	ph_t_obstacle = record
		slice    : shortint; {< Slice of the polygon where obstacle is. }
		distance : single;   {< Distance to the polygon. }
		depth    : single;   {< Depth (heigth) of the obstacle trapezoid. }
	end;

procedure createObstacle   (slice  : shortint;
                            depth  : single;
                            offset : single);

function  collisionAt      (pos  : single) : boolean;

procedure resetObstacles   ();
procedure moveObstacles    (dist : single);
procedure updateObstacles  ();
procedure drawObstacles    ();

//==============//
  IMPLEMENTATION
//==============//

uses
	{$IFDEF USE_ZENGL_STATIC}
	zgl_fx,
	zgl_primitives_2d,
	{$ELSE}
	zglHeader,
	{$ENDIF}
	ph_colors,
	ph_camera,
	ph_polygon,
	ph_player;

const
	MAX_OBSTACLES      = 256;
	OBSTACLE_SPAWN_POS = 12;

var
	obstacles    : array[0..(MAX_OBSTACLES - 1)] of ph_t_obstacle;
	first        : smallint;
	count        : smallint;
	points_count : smallint;
	tl_obstacles : array[0..(6 * MAX_OBSTACLES - 1)] of TPoint;
	out_points   : array[0..5] of TPoint;


procedure pushObstacle   (const o : ph_t_obstacle); forward;
procedure popObstacle    (); forward;
procedure updateObstacle (const o : ph_t_obstacle; dest: PPoint); forward;


{ ----------------------------------------------------------------------------
  Procedure to create an obstacle and push it to the back of the queue.
  ---------------------------------------------------------------------------- }
procedure createObstacle(slice  : shortint;
                         depth  : single;
                         offset : single);
var
	o : ph_t_obstacle;
begin

	o.slice    := slice;
	o.depth    := depth;
	o.distance := OBSTACLE_SPAWN_POS + offset;

	pushObstacle(o);

end;

{ ----------------------------------------------------------------------------
  Function to check for collision in a given position. The position must be
  given as a simple angle in radians (between 0 and 2*pi).

  pos     Position to check for collision (in radians: 0 <= pos < 2*pi).

  return  Returns whether there is a collision in that position.
  ---------------------------------------------------------------------------- }
function collisionAt(pos : single) : boolean;
var
	i               : integer;
	pt_ln_dist      : single;
	p0, p1, p2, d12 : TPoint;
begin

	collisionAt := false;

	for i := first to (first + count - 1) do
	begin

		with obstacles[i mod MAX_OBSTACLES] do
		begin

			if distance > (PLAYER_DIST - getPolygonApothem) then
				break
			else if slice = sliceAt(pos) then
			begin

				p0.x := PLAYER_DIST    * cos(pos);
				p0.y := PLAYER_DIST    * sin(pos);
				p1.x := POLYGON_RADIUS * cos(getSliceAngle(slice));
				p1.y := POLYGON_RADIUS * sin(getSliceAngle(slice));
				p2.x := POLYGON_RADIUS * cos(getSliceAngle(slice + 1));
				p2.y := POLYGON_RADIUS * sin(getSliceAngle(slice + 1));
				d12.x := p2.x - p1.x;
				d12.y := p2.y - p1.y;

				pt_ln_dist := abs(   (   d12.y * p0.x - d12.x * p0.y
				                       + p2.x  * p1.y - p2.y  * p1.x )
				                   / ( sqrt(sqr(d12.y) + sqr(d12.x)) ) );

				if (     (distance + depth) >  pt_ln_dist)
				     and (pt_ln_dist        >= (distance)  ) then
				begin
					collisionAt := true;
					break;
				end;

			end;
		end;
	end;
end;

{ ----------------------------------------------------------------------------
  Procedure to clear the obstacles queue.
  ---------------------------------------------------------------------------- }
procedure resetObstacles();
var
	i : integer;
begin

	first        := 0;
	count        := 0;
	points_count := 0;

	{ Points to be copied to invisible obstacles (polygon changes) so they are
	  not drawn to the screen. }
	for i := 0 to 5 do
	begin
		out_points[i].x := 32768;
		out_points[i].y := 32768;
	end;

end;

{ ----------------------------------------------------------------------------
  Procedure to move all the obstacles. Obstacles that go entirely inside the
  polygon area are destroyed (removed from the queue).

  dist  Distance to move the obstacles.
  ---------------------------------------------------------------------------- }
procedure moveObstacles(dist : single);
var
	i : integer;
begin

	{ Return if movement is null }
	if dist = 0 then
		exit;

	{ Move obstacles }
	for i := 0 to count - 1 do
	begin
		with obstacles[(first + i) mod MAX_OBSTACLES] do
			distance := distance - dist;
	end;

	{ Remove obstacles that go past the polygon }
	while ( (count > 0) and
	      ( (   obstacles[first].distance
	          + obstacles[first].depth    ) <= 0 ) ) do
	begin
		if obstacles[first].slice < 0 then
			changePolygon(-(obstacles[first].slice));
		popObstacle;
	end;

end;

{ ----------------------------------------------------------------------------
  Procedure to update the geometry of all obstacles. Should be called every
  frame before drawing the obstacles.
  ---------------------------------------------------------------------------- }
procedure updateObstacles();
var
	i: integer;
begin

	for i := 0 to count - 1 do
	begin

		updateObstacle( obstacles[(first + i) mod MAX_OBSTACLES],
		                @tl_obstacles[i * 6] );
	end;

	points_count := count * 6;

end;

{ ----------------------------------------------------------------------------
  Procedure to draw all obstacles.
  ---------------------------------------------------------------------------- }
procedure drawObstacles();
begin

	pr2d_TriList( nil, @tl_obstacles   , nil        ,
	              0  , points_count - 1, getColor(0),
	              255, PR2D_FILL or FX_BLEND );

end;

{ ----------------------------------------------------------------------------
  Procedure to push an obstacle to the back of the queue.

  o  Obstacle to be pushed.
  ---------------------------------------------------------------------------- }
procedure pushObstacle(const o : ph_t_obstacle);
begin

	move(o, obstacles[(first + count) mod MAX_OBSTACLES], sizeOf(ph_t_obstacle));
	inc(count);

end;

{ ----------------------------------------------------------------------------
  Procedure to pop an obstacle from the front of the queue.
  ---------------------------------------------------------------------------- }
procedure popObstacle();
begin

	if count > 0 then
	begin
		dec(count);
		first := (first + 1) mod MAX_OBSTACLES;
	end;

end;

{ ----------------------------------------------------------------------------
  Procedure to update the geometry of a single obstacle.

  o     Obstacle to update.
  dest  Pointer to the first point of the array corresponding to the obstacle.
  ---------------------------------------------------------------------------- }
procedure updateObstacle(const o : ph_t_obstacle; dest: PPoint);
var
	start , stop       : single;
	angle1, sin1, cos1 : single;
	angle2, sin2, cos2 : single;
	cos_angle          : single;
begin

	{ For polygon changes }
	if o.slice < 0 then
	begin
		move(out_points, dest^, 6 * sizeof(TPoint));
		exit;
	end;

	angle1    := getSliceAngle(o.slice);
	angle2    := getSliceAngle(o.slice + 1);
	sin1      := sin(angle1);
	cos1      := cos(angle1);
	sin2      := sin(angle2);
	cos2      := cos(angle2);
	cos_angle := cos((angle2 - angle1) / 2);

	if o.distance < 0 then
		start := POLYGON_RADIUS
	else
		start := POLYGON_RADIUS + o.distance / cos_angle;

	if (o.distance + o.depth) < 0 then
		stop := POLYGON_RADIUS
	else
		stop := POLYGON_RADIUS + (o.distance + o.depth) / cos_angle;

	start := start + getPolygonExpansion / start;
	stop  := stop  + getPolygonExpansion / stop;

	dest[0].x := start * cos1;
	dest[0].y := start * sin1;
	dest[1].x := start * cos2;
	dest[1].y := start * sin2;
	dest[2].x := stop  * cos1;
	dest[2].y := stop  * sin1;
	dest[5].x := stop  * cos2;
	dest[5].y := stop  * sin2;

	projectPoint(dest[0]);
	projectPoint(dest[1]);
	projectPoint(dest[2]);
	projectPoint(dest[5]);

	dest[3].x := dest[1].x;
	dest[3].y := dest[1].y;
	dest[4].x := dest[2].x;
	dest[4].y := dest[2].y;

end;

end.

