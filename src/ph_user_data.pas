{ ----------------------------------------------------------------------------
  File: ph_user_data.pas

  Unit that implements functions to get the path to the various user folders
  used by the game.
  ----------------------------------------------------------------------------
  Copyright 2016, 2017 Eduardo Mezêncio.

  This file is part of Pascal Hexagon.

  Pascal Hexagon is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Pascal Hexagon is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Pascal Hexagon.  If not, see <http://www.gnu.org/licenses/>.
  ---------------------------------------------------------------------------- }

unit ph_user_data;

//=========//
  INTERFACE
//=========//

procedure initUserData         ();
function  getUserStagesDir     () : utf8String;
function  getUserMusicDir      () : utf8String;
function  getUserMusicAudioDir () : utf8String;
function  getUserMusicInfoDir  () : utf8String;
function  getUserSaveDir       () : utf8String;

//==============//
  IMPLEMENTATION
//==============//

uses
	sysutils,
	{$IFDEF USE_ZENGL_STATIC}
	zgl_log,
	zgl_main,
	zgl_utils,
	{$ELSE}
	zglHeader,
	{$ENDIF}
	ph_global;

var
	user_dir             : utf8String;
	user_stages_dir      : utf8String;
	user_music_dir       : utf8String;
	user_music_audio_dir : utf8String;
	user_music_info_dir  : utf8String;
	user_save_dir        : utf8String;

{ ----------------------------------------------------------------------------
  Procedure to initialize the user folders. Will put the path to the user
  folders in strings and create the folders if they don't exist.
  ---------------------------------------------------------------------------- }
procedure initUserData();
var
	str : string;
begin
	{ ZenGL's zgl_get(DIRECTORY_HOME) returns the folder recommended to store
	  application preferences for the current user. }
	user_dir := utf8_copy(PAnsiChar(zgl_get(DIRECTORY_HOME))) + 'pascalhexagon' + SLASH;
	user_stages_dir      := user_dir + 'stages';
	user_music_dir       := user_dir + 'music';
	user_music_audio_dir := user_music_dir + SLASH + 'audio';
	user_music_info_dir  := user_music_dir + SLASH + 'info';
	user_save_dir        := user_dir + 'save';

	if not directoryExists(user_dir) then
	begin
		if createDir(user_dir)
			then str := 'Created'
			else str := 'Failed to create';
	end
	else str := 'Using';

	log_add('pascalhexagon: ' + str + ' ' + user_dir + ' for user data.');

	if not directoryExists(user_stages_dir     ) then createDir(user_stages_dir     );
	if not directoryExists(user_music_dir      ) then createDir(user_music_dir      );
	if not directoryExists(user_music_audio_dir) then createDir(user_music_audio_dir);
	if not directoryExists(user_music_info_dir ) then createDir(user_music_info_dir );
	if not directoryExists(user_save_dir       ) then createDir(user_save_dir       );

end;

{ ----------------------------------------------------------------------------
  Function to return the path to the user stages folder.

  return  Returns the user stages folder's path.
  ---------------------------------------------------------------------------- }
function getUserStagesDir() : utf8String;
begin

	getUserStagesDir := user_stages_dir + SLASH;

end;

{ ----------------------------------------------------------------------------
  Function to return the path to the user music folder.

  return  Returns the user music folder's path.
  ---------------------------------------------------------------------------- }
function getUserMusicDir() : utf8String;
begin

	getUserMusicDir := user_music_dir + SLASH;

end;

{ ----------------------------------------------------------------------------
  Function to return the path to the user music audio folder.

  return  Returns the user music audio folder's path.
  ---------------------------------------------------------------------------- }
function getUserMusicAudioDir() : utf8String;
begin

	getUserMusicAudioDir := user_music_audio_dir + SLASH;

end;

{ ----------------------------------------------------------------------------
  Function to return the path to the user music info folder.

  return  Returns the user music info folder's path.
  ---------------------------------------------------------------------------- }
function getUserMusicInfoDir() : utf8String;
begin

	getUserMusicInfoDir := user_music_info_dir + SLASH;

end;

{ ----------------------------------------------------------------------------
  Function to return the path to the user saved games folder.

  return  Returns the user saved games folder's path.
  ---------------------------------------------------------------------------- }
function getUserSaveDir() : utf8String;
begin

	getUserSaveDir := user_save_dir + SLASH;

end;

end.
