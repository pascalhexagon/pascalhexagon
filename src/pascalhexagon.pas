{ ----------------------------------------------------------------------------
  File: pascalhexagon.pas

  This is the main program for Pascal Hexagon.
  ----------------------------------------------------------------------------
  Copyright 2016, 2017 Eduardo Mezêncio.

  This file is part of Pascal Hexagon.

  Pascal Hexagon is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Pascal Hexagon is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Pascal Hexagon.  If not, see <http://www.gnu.org/licenses/>.
  ---------------------------------------------------------------------------- }

program pascalhexagon;

{$IFDEF MSWINDOWS}
{$DEFINE WINDOWS}
{$ENDIF}

uses
	sysutils,
	{$IFDEF USE_ZENGL_STATIC}
	zgl_main,
	zgl_screen,
	zgl_window,
	zgl_timers,
	zgl_sound,
	zgl_utils,
	{$ELSE}
	zglHeader,
	{$ENDIF}
	ph_global,
	ph_user_data,
	ph_resources,
	ph_camera,
	ph_stage,
	ph_hud,
	ph_st_title;

{ ----------------------------------------------------------------------------
  Procedure to be passed to ZenGL as callback for the load event. It is called
  once when ZenGL is initialized.
  ---------------------------------------------------------------------------- }
procedure init;
begin

	randomize;

	defaultFormatSettings.decimalSeparator := '.';

	zgl_Disable(APP_USE_AUTOPAUSE);
	zgl_Disable(COLOR_BUFFER_CLEAR);

	snd_init;

	initUserData;

	loadResourcesInfo;
	loadMusicInfo;
	loadFonts;
	loadSounds;
	loadStagesFromDisk;

	initHud;

	loadTitle;

end;

{ ----------------------------------------------------------------------------
  Procedure to be passed to ZenGL as callback for the exit event. It is called
  once when ZenGL exits.
  ---------------------------------------------------------------------------- }
procedure quit;
begin

	freeResources;

end;

{ ----------------------------------------------------------------------------
  Procedure to update the window title, to be used with a timer.
  ---------------------------------------------------------------------------- }
procedure updateWindowTitle;
begin

	wnd_setCaption(  WINDOW_TITLE + ' [ FPS: '
	               + u_intToStr(zgl_get(RENDER_FPS)) + ' ]');

end;

begin

	{$IFNDEF USE_ZENGL_STATIC}
	if not zglLoad(libZenGL) then exit;
	{$ENDIF}

	{ Comment if you don't want FPS in window caption }
	{ timer_add(@updateWindowTitle, 1000); }

	zgl_reg(SYS_LOAD, @init);
	zgl_reg(SYS_EXIT, @quit);

	wnd_setCaption(WINDOW_TITLE);
	wnd_showCursor(false);

	setScreenSize(zgl_get(DESKTOP_WIDTH),
	              zgl_get(DESKTOP_HEIGHT));
	scr_setOptions(getScreenWidth, getScreenHeight,
	               REFRESH_DEFAULT, false, false);

	zgl_init;

end.

