{ ----------------------------------------------------------------------------
  File: ph_st_title.pas

  Unit for the title state. The interface contains only the procedure to
  activate this state. This procedure changes the update and draw callbacks to
  the ones implemented in this file.
  ----------------------------------------------------------------------------
  Copyright 2016, 2017 Eduardo Mezêncio.

  This file is part of Pascal Hexagon.

  Pascal Hexagon is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Pascal Hexagon is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Pascal Hexagon.  If not, see <http://www.gnu.org/licenses/>.
  ---------------------------------------------------------------------------- }

unit ph_st_title;

//=========//
  INTERFACE
//=========//

procedure loadTitle();

//==============//
  IMPLEMENTATION
//==============//

uses
	{$IFDEF USE_ZENGL_STATIC}
	zgl_keyboard,
	zgl_main,
	zgl_render_2d,
	{$ELSE}
	zglHeader,
	{$ENDIF}
	ph_global,
	ph_helper,
	ph_resources,
	ph_lfo,
	ph_colors,
	ph_camera,
	ph_polygon,
	ph_stage,
	ph_hud,
	ph_st_stage_select;


procedure init   ();            forward;
procedure update (dt : double); forward;
procedure draw   ();            forward;

{ ----------------------------------------------------------------------------
  Procedure to set the state to the title state, changing the callback
  functions update and draw accordingly.
  ---------------------------------------------------------------------------- }
procedure loadTitle();
begin

	zgl_reg(SYS_UPDATE, @update);
	zgl_reg(SYS_DRAW  , @draw  );

	init;

end;

{ ----------------------------------------------------------------------------
  Procedure to initialize the title state.
  ---------------------------------------------------------------------------- }
procedure init();
begin

	setTiltAngle(-90);
	setTiltMagnitude(0.055);
	calculateTilt;

	resetCamera;
	resetPolygon;

	setGameColors(random(360) / 360, random(360) / 360, 0.2, 0.5, 0.75, 0.5);

	setRotationSpeed(30);

	playVoice(VOICE_PH);

end;

{ ----------------------------------------------------------------------------
  Procedure to be used as update callback for the title state.

  dt  Time elapsed since last frame (in milliseconds).
  ---------------------------------------------------------------------------- }
procedure update(dt : double);
begin

	if key_press ( K_ENTER ) then
	begin
		playSfx(SFX_START);
		setCurrentStage(0);
		loadStageSelect;
	end
	else if key_press ( K_ESCAPE ) then zgl_exit;
	key_clearState;

	rotateCamera(dt);
	calculateTilt;
	updatePolygon(dt);

	updateHudColor;

end;

{ ----------------------------------------------------------------------------
  Procedure to be used as draw callback for the title state.
  ---------------------------------------------------------------------------- }
procedure draw();
begin

	batch2d_begin;

	useCamera;
	drawPolygon;

	useNoCamera;
	drawTitleHud;

	batch2d_end;

end;

end.
